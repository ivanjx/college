using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class duplicateobstacle : MonoBehaviour
{
    public bool isGameOver = false;
    public GameObject obstaclePrefab;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(duplicate());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator duplicate()
    {
        while (!isGameOver)
        {
            float distance = Random.Range(0.5F, 1.5F);
            yield return new WaitForSeconds(distance);

            if (!isGameOver)
            {
                GameObject obstacle = Instantiate(
                    obstaclePrefab,
                    transform.position,
                    transform.rotation,
                    transform);
                obstacle.name = "obstacle";
            }
        }
    }
}
