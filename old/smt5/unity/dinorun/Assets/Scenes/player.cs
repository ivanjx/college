using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    public int jumpHeight;
    public GameObject ground;
    public GameObject parentObstacle;
    public GameObject gameOverText;
    public GameObject resetButton;
    public GameObject scoreText;
    private bool _isOnTheGround;
    private int _score;


    // Start is called before the first frame update
    void Start()
    {
        _isOnTheGround = true;
        _score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOverText.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Space) && _isOnTheGround)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, jumpHeight);
            }
            
            ++_score;
            scoreText.GetComponent<Text>().text = "Score: " + _score;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        _isOnTheGround = true;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        _isOnTheGround = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "obstacle")
        {
            GetComponent<Animator>().enabled = false;
            ground.GetComponent<Animator>().enabled = false;
            parentObstacle.GetComponent<duplicateobstacle>().isGameOver = true;

            for (int i = 0; i < parentObstacle.transform.childCount; ++i)
            {
                parentObstacle.transform.GetChild(i).GetComponent<obstacle>().speed = 0;
            }

            gameOverText.SetActive(true);
            resetButton.SetActive(true);
            _score = 0;
        }
    }
}
