using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : MonoBehaviour
{
    const int SPEED = 20;

    public Rigidbody projectile;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Rigidbody instantiatedProjectile = Instantiate(
                projectile,
                transform.position,
                transform.rotation);
            instantiatedProjectile.velocity = transform.TransformDirection(
                new Vector3(0, 0, SPEED));
        }
    }
}
