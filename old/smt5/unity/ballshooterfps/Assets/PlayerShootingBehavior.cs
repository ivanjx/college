using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootingBehavior : MonoBehaviour
{
    public GameObject projectile;
    public GameObject weapon;
    GameObject _instProjectile;
    const float MAX_SCALE = 3;
    float _instProjScale = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void UpdateProjectileScale()
    {
        _instProjectile.transform.localScale =
            new Vector3(_instProjScale, _instProjScale, _instProjScale);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (_instProjectile == null)
            {
                _instProjectile = Instantiate(
                    projectile,
                    weapon.transform.position,
                    weapon.transform.rotation);
                _instProjectile.tag = "projectile";
            }

            if (_instProjScale < MAX_SCALE)
            {
                _instProjScale += 0.05F;
            }

            UpdateProjectileScale();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            _instProjectile.GetComponent<Rigidbody>().AddRelativeForce(
                Vector3.forward * 10000);
            Destroy(_instProjectile, 3);
            _instProjectile = null;
            _instProjScale = 0;
        }
    }
}
