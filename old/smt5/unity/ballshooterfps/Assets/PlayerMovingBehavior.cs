using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovingBehavior : MonoBehaviour
{
    public float movingSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool isMoving = false;
        Vector3 vec = new Vector3();

        if (Input.GetKey(KeyCode.UpArrow))
        {
            isMoving = true;
            vec = Vector3.forward;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            isMoving = true;
            vec = Vector3.back;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            isMoving = true;
            vec = Vector3.left;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            isMoving = true;
            vec = Vector3.right;
        }

        if (isMoving)
        {
            transform.Translate(
                vec * movingSpeed * Time.deltaTime,
                Space.World);
        }
    }
}
