using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    int _score = 0;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "projectile")
        {
            ++_score;
        }
    }

    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 400, 200));
        GUILayout.Label("Score: " + _score);
        GUILayout.EndArea();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
