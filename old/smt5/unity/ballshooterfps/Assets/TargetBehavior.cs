using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBehavior : MonoBehaviour
{
    public GameObject player;
    public float movingSpeed;
    public float turnSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 gapPos = player.transform.position - transform.position;
        gapPos = new Vector3(
            gapPos.x,
            0,
            gapPos.z);
        Quaternion lookRot = Quaternion.LookRotation(gapPos);
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            lookRot,
            turnSpeed);
        transform.Translate(Vector3.back * movingSpeed * Time.deltaTime);
    }
}
