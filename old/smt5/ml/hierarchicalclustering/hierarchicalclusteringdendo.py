import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.preprocessing import StandardScaler
from scipy.cluster.hierarchy import cophenet, dendrogram, linkage
from scipy.spatial.distance import pdist

iris = datasets.load_iris()
iris = pd.DataFrame(
    data=np.c_[iris["data"], iris["target"]],
    columns=iris["feature_names"] + ["species"]
)

iris.columns = iris.columns.str.replace(" ", "")
iris.head()

x = iris.iloc[:, :3]
y = iris["species"]
sc = StandardScaler()
sc.fit(x)
x = sc.transform(x)

z = linkage(x, "ward")
c, coph_dists = cophenet(z, pdist(x))

plt.figure(figsize=(25, 10))
plt.title("Agglomerative Hierarchical Clustering Dendogram")
plt.xlabel("Sample Index")
plt.ylabel("Distance")
dendrogram(
    z,
    leaf_rotation=90.,
    leaf_font_size=8.
)
plt.tight_layout()
plt.show()