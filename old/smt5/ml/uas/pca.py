import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

# Load data.
dataset = pd.read_csv(
    "Pizza.csv",
    names=["Brand","mois","prot","fat","ash","sodium","carb","cal"],
    header=0,
    sep=","
)
features = dataset.iloc[:, 1:].values
label = dataset.iloc[:, 0].values

# Getting cummulative variance.
features_std = StandardScaler().fit_transform(features)
cov_mat = np.cov(features_std.T)
eig_vals, eig_vecs = np.linalg.eig(cov_mat)
eig_pairs = [
    (np.abs(eig_vals[i]), eig_vecs[:, i])
    for i in range(len(eig_vals))
]
tot = sum(eig_vals)
var_exp = [
    (i / tot) * 100
    for i in sorted(eig_vals, reverse=True)
]
cum_var_exp = np.cumsum(var_exp)
print("Cummulative Variance Explained", cum_var_exp)

# Plotting.
plt.figure(figsize=(6, 4))
plt.bar(
    range(7),
    var_exp,
    alpha=0.5,
    align="center",
    label="Individual explained variance"

)
plt.step(
    range(7),
    cum_var_exp,
    where="mid",
    label="Cumulative explained variance"
)
plt.ylabel("Explained variance ratio")
plt.xlabel("Principal components")
plt.legend(loc="best")
plt.tight_layout()
plt.show()

# Summary.
n_features_reduced = len([x for x in cum_var_exp if x <= 99])
print("Jumlah fitur terbaik: {}".format(n_features_reduced))