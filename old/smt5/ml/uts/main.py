import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

# Load data.
print("Loading data...")
dataset = pd.read_csv(
    "iris.csv",
    names=["sepals-length", "sepals-width", "label"],
    header=0,
    sep=",")

# Plot raw data.
print("Plotting raw data...")
sns.scatterplot(
    x="sepals-length",
    y="sepals-width",
    hue="label",
    data=dataset)
plt.figure(1)
plt.show()

# Get features.
features = dataset.iloc[:, 0:2].values
label = dataset.iloc[:, 2].values

# Split training/test data.
print("Splitting data...")
x_train, x_test, y_train, y_test = train_test_split(
    features,
    label,
    test_size=1/3,
    random_state=0)

# Train.
K = 3
print("K: {}".format(K))
print("Training model...")
model = KNeighborsClassifier(n_neighbors=K)
model.fit(x_train, y_train)

# Test model.
print("Testing model...")
acc = model.score(x_train, y_train)
print("Accuracy: {}%".format(acc * 100))

# Prompt.
print(".:: Prediction ::.")
in_length = float(input("Sepal length: "))
in_width = float(input("Sepal width: "))
pred = model.predict([[ in_length, in_width ]])
print("Result: {}".format(pred[0]))