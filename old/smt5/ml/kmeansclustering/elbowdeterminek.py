import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from scipy.spatial.distance import cdist, pdist

iris = datasets.load_iris()
iris = pd.DataFrame(
    data=np.c_[iris["data"], iris["target"]],
    columns=iris["feature_names"] + ["species"]
)
iris.columns = iris.columns.str.replace(" ", "")
iris.head()

x = iris.iloc[:, :3]
y = iris["species"]
sc = StandardScaler()
sc.fit(x)
x = sc.transform(x)

k = range(1, 10)
km = [KMeans(n_clusters=_k).fit(x) for _k in k]
centroids = [_k.cluster_centers_ for _k in km]

dk = [cdist(x, cent, "euclidean") for cent in centroids]
c_idx = [np.argmin(d, axis=1) for d in dk]
dist = [np.min(d, axis=1) for d in dk]
avg_within_ss = [sum(d) / x.shape[0] for d in dist]

wcss = [sum(d ** 2) for d in dist]
tss = sum(pdist(x) ** 2) / x.shape[0]
bss = tss - wcss
var_explained = bss / tss * 100

k_idx = 2
plt.figure(figsize=(10, 4))

plt.subplot(1, 2, 1)
plt.plot(k, avg_within_ss, "b*-")
plt.plot(
    k[k_idx],
    avg_within_ss[k_idx],
    marker="o",
    markersize=12,
    markeredgewidth=2,
    markeredgecolor="r",
    markerfacecolor="None"
)
plt.grid(True)
plt.xlabel("Number of clusters")
plt.ylabel("Average within-cluster sum of squares")
plt.title("Elbow for KMeans Clustering")

plt.subplot(1, 2, 2)
plt.plot(k, var_explained, "b*-")
plt.plot(
    k[k_idx],
    var_explained[k_idx],
    marker="o",
    markersize=12,
    markeredgewidth=2,
    markeredgecolor="r",
    markerfacecolor="None"
)
plt.grid(True)
plt.xlabel("Number of clusters")
plt.ylabel("Percentage of variance explained")
plt.title("Elbow for KMeans Clustering")

plt.tight_layout()
plt.show()