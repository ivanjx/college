import numpy as np
import pandas as pd
from sklearn import datasets, metrics
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler

iris = datasets.load_iris()
iris = pd.DataFrame(
    data=np.c_[iris["data"], iris["target"]],
    columns=iris["feature_names"] + ["species"]
)

print("Data awal iris:")
print(iris)

iris.columns = iris.columns.str.replace(" ", "")
iris.head()

x = iris.iloc[:, :3]
y = iris["species"]
sc = StandardScaler()
sc.fit(x)
x = sc.transform(x)

model = KMeans(n_clusters=3, random_state=11)
model.fit(x)
print("Label hasil clustering KMeans:")
print(model.labels_)

iris["pred_species"] = np.choose(
    model.labels_,
    [1, 0, 2]).astype(np.int64)

print(
    "Accuracy:",
    metrics.accuracy_score(
        iris["species"],
        iris["pred_species"]
    )
)
print("Classificiation report:")
print(
    metrics.classification_report(
        iris["species"],
        iris["pred_species"]
    )
)

print(
    "Homogeneity score:",
    metrics.homogeneity_score(
        y,
        iris["pred_species"]
    )
)
print(
    "Homogeneity, completeness, v-measure:",
    metrics.homogeneity_completeness_v_measure(
        y,
        iris["pred_species"],
        beta=1.0
    )
)

print("Cetak data iris dengan full rows:")
pd.set_option("display.max_rows", None)
print(iris)

print("Cetak data iris dengan sample acak sebesar 0.3:")
print(iris.sample(frac=0.3))