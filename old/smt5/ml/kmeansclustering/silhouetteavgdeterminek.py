import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import silhouette_samples, silhouette_score
from matplotlib import cm

iris = datasets.load_iris()
iris = pd.DataFrame(
    data=np.c_[iris["data"], iris["target"]],
    columns=iris["feature_names"] + ["species"]
)

iris.columns = iris.columns.str.replace(" ", "")
iris.head()

x = iris.iloc[:, :3]
sc = StandardScaler()
sc.fit(x)
x = sc.transform(x)

score = []
for n_clusters in range(2, 10):
    kmeans = KMeans(n_clusters=n_clusters)
    kmeans.fit(x)
    score.append(silhouette_score(
        x,
        kmeans.labels_
    ))

plt.figure(figsize=(10, 4))
plt.subplot(1, 2, 1)
plt.plot(score)
plt.grid(True)
plt.ylabel("Silhouette Score")
plt.xlabel("K")
plt.title("Silhouette for K-Means")

model = KMeans(
    n_clusters=3,
    init="k-means++",
    n_init=10,
    random_state=0
)
model.fit_predict(x)
cluster_labels = np.unique(model.labels_)
n_clusters = cluster_labels.shape[0]

silhouette_vals = silhouette_samples(x, model.labels_)

plt.subplot(1, 2, 2)
y_lower, y_upper = 0, 0
y_ticks = []
cmap = cm.get_cmap("Spectral")
for i, c in enumerate(cluster_labels):
    c_silhouette_vals = silhouette_vals[model.labels_ == c]
    c_silhouette_vals.sort()
    y_upper = y_lower + len(c_silhouette_vals)
    color = cmap(float(i) / n_clusters)
    plt.barh(
        range(y_lower, y_upper),
        c_silhouette_vals,
        facecolor=color,
        edgecolor=color,
        alpha=0.7
    )
    y_ticks.append((y_lower + y_upper) / 2)
    y_lower = y_upper + 10

silhouette_avg = np.mean(silhouette_vals)
plt.yticks(y_ticks, cluster_labels + 1)
plt.axvline(
    x=silhouette_avg,
    color="red",
    linestyle="--"
)
plt.ylabel("Cluster")
plt.xlabel("Silhouette Coefficient")
plt.title("Silhouette for K-Means")
plt.show()
