import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

dataset = pd.read_csv(
    "hipertensi.csv",
    names=["umur (Tahun)", "kegemukan (KG)", "label"],
    header=0,
    sep=",")

sns.scatterplot(
    x="umur (Tahun)",
    y="kegemukan (KG)",
    hue="label",
    data=dataset)
plt.figure(1)
plt.show()