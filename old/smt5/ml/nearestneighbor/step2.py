import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

dataset = pd.read_csv(
    "hipertensi.csv",
    names=["umur", "kegemukan", "class"],
    header=0)
fitur = dataset.iloc[:, 0:2].values
label = dataset.iloc[:, -1].values

x_train, x_test, y_train, y_test = train_test_split(
    fitur,
    label,
    test_size=1/3,
    random_state=0)
print("Data training:")
print(x_train)
print("Data test:")
print(x_test)

model = KNeighborsClassifier(n_neighbors=3)
model.fit(x_train, y_train)

akurasi = model.score(x_train, y_train)
print("Akurasi model: {}".format(akurasi))