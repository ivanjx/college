import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

dataset = pd.read_csv(
    "hipertensi.csv",
    names=["umur", "kegemukan", "class"],
    header=0)
fitur = dataset.iloc[:, 0:2].values
label = dataset.iloc[:, -1].values

model = KNeighborsClassifier(n_neighbors=3)
model.fit(fitur, label)

umur = float(input("Umur anda: "))
beratBadan = float(input("Berat badan: "))
pred = model.predict([[umur, beratBadan]])

if pred == 0:
    print("Sehat")
elif pred == 1:
    print("Hipertensi")
else:
    print("Unknown")