package com.ivan.portscanner;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
        Scanner input = new Scanner(System.in);
        
        System.out.print("IP address target: ");
        String target = input.next();

        System.out.print("Scan from port: ");
        int portStart = input.nextInt();

        System.out.print("Until port: ");
        int portEnd = input.nextInt();

        input.close();

        InetAddress targetAddress = InetAddress.getByName(target);
        System.out.println("Target address: " + targetAddress);
        String targetHostName = targetAddress.getHostName();
        System.out.println("Target host name: " + targetHostName);

        for (int port = portStart; port <= portEnd; ++port)
        {
            try
            {
                Socket socket = new Socket();
                socket.connect(
                    new InetSocketAddress(
                        targetAddress,
                        port),
                    100);
                socket.close();
                System.out.println(
                    targetHostName +
                    " is listening on port " +
                    port);
            }
            catch (Exception ex) { }
        }
    }
}
