package com.ivan.multitcpechoserver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler extends Thread
{
    private Socket _client;

    public ClientHandler(Socket client)
    {
        _client = client;
    }
    
    @Override
    public void run()
    {
        try
        {
            BufferedReader clientIn = new BufferedReader(
                new InputStreamReader(
                    _client.getInputStream()));
            PrintWriter clientOut = new PrintWriter(
                _client.getOutputStream(),
                true);
            
            while (true)
            {
                String received = clientIn.readLine();
                System.out.println(received);
                clientOut.println("ECHO : " + received);

                if (received.equals("quit"))
                {
                    break;
                }
            }

            System.out.println("Closing connection");
            _client.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
    }
}
