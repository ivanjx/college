package com.ivan.multitcpechoserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class App 
{
    private static final int PORT = 1234;

    public static void main( String[] args )
    {
        try
        {
            System.out.println("Opening port " + PORT + " ...");
            ServerSocket server = new ServerSocket(PORT);

            while (true)
            {
                Socket client = server.accept();
                ClientHandler handler = new ClientHandler(client);
                handler.start();
            }
        }
        catch (IOException ex)
        {
            System.out.println("Unable to attach port");
        }
    }
}
