package com.ivan.jdbcsocket;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    private static final String URL = "jdbc:mysql://localhost:3306/test?user=root&password=12345678";

    private static void printResult(ResultSet rs) throws SQLException {
        System.out.println("== Current Data ==");
        while (rs.next()) {
            String nim = rs.getString(1);
            String nama = rs.getString(2);
            int angkatan = rs.getInt(3);
            System.out.println("NIM      : " + nim);
            System.out.println("Nama     : " + nama);
            System.out.println("Angkatan : " + angkatan);
            System.out.println();
        }
        System.out.println("==================");
    }

    public static void main(String[] args) {
        System.out.println("Connecting...");
        try (
            Connection conn = DriverManager.getConnection(URL);
            Statement stmt = conn.createStatement();
        ) {
            System.out.println("Creating table...");
            stmt.execute("DROP TABLE IF EXISTS mahasiswa");
            stmt.execute("CREATE TABLE mahasiswa(nim VARCHAR(8) PRIMARY KEY NOT NULL, nama VARCHAR(100) NOT NULL, angkatan INTEGER NOT NULL)");

            System.out.println("Inserting data...");
            int updateResult = stmt.executeUpdate(
                "INSERT INTO mahasiswa VALUES" +
                "('19360016', 'Ivan Juan Kara', 2019)," +
                "('19360017', 'John Smith', 2019)," +
                "('19360018', 'Jane Smith', 2019)"
            );

            if (updateResult != 3) {
                throw new Exception("Unable to insert data");
            }

            ResultSet rs = stmt.executeQuery("SELECT * FROM mahasiswa");
            printResult(rs);

            System.out.println("Updating 2nd data...");
            updateResult = stmt.executeUpdate("UPDATE mahasiswa SET nama='John Kara', angkatan=2020 WHERE nim='19360017'");

            if (updateResult != 1) {
                throw new Exception("Unable to update 2nd data");
            }

            rs = stmt.executeQuery("SELECT * FROM mahasiswa");
            printResult(rs);

            System.out.println("Deleting last data...");
            updateResult = stmt.executeUpdate("DELETE FROM mahasiswa WHERE nim='19360018'");

            if (updateResult != 1) {
                throw new Exception("Unable to update 2nd data");
            }

            rs = stmt.executeQuery("SELECT * FROM mahasiswa");
            printResult(rs);

            System.out.println("Done.");
        } catch (Exception ex) {
            System.out.println("Error:");
            System.out.println(ex.toString());
        }
    }
}
