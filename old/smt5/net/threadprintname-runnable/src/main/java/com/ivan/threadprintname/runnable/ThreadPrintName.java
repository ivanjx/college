package com.ivan.threadprintname.runnable;

public class ThreadPrintName implements Runnable
{
    Thread _thread;

    public ThreadPrintName(String name)
    {
        _thread = new Thread(this, name);
        _thread.start();
    }

    @Override
    public void run()
    {
        String name = _thread.getName();

        for (int i = 0; i < 10000; ++i)
        {
            System.out.print(name);

            try
            {
                Thread.sleep(0);
            }
            catch (InterruptedException ex) { }
        }
    }
}
