package com.ivan.rmihello.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import com.ivan.rmihello.contract.SayHelloInterface;

public class App 
{
    public static void main( String[] args ) throws MalformedURLException, RemoteException, NotBoundException
    {
        SayHelloInterface sayHello = (SayHelloInterface)Naming.lookup(
            "rmi://localhost:1099/sayHello");
        System.out.println("Client berhasil terkoneksi ke server");
        System.out.println("Untuk berhenti ketik close");

        Scanner userIn = new Scanner(System.in);

        while (true)
        {
            System.out.print("Nama anda: ");
            String nama = userIn.nextLine();

            if (nama.toLowerCase().equals("close"))
            {
                break;
            }
            
            String response = sayHello.sayHello(nama);
            System.out.println(response);
        }

        userIn.close();
    }
}
