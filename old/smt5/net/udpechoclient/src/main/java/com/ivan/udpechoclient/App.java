package com.ivan.udpechoclient;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class App 
{
    private static final int PORT = 1234;
    private static final int BUFFER_SIZE = 256;

    public static void main( String[] args )
    {
        try
        {
            InetAddress serverHost = InetAddress.getLocalHost();
            DatagramSocket serverSocket = new DatagramSocket();
            Scanner userIn = new Scanner(System.in);
            byte[] buffer = new byte[BUFFER_SIZE];

            while (true)
            {
                System.out.print("Enter message: ");
                String message = userIn.nextLine();

                if (message.equals("CLOSE"))
                {
                    break;
                }

                DatagramPacket pktOut = new DatagramPacket(
                    message.getBytes(),
                    message.length(),
                    serverHost,
                    PORT);
                serverSocket.send(pktOut);
                
                DatagramPacket pktIn = new DatagramPacket(
                    buffer,
                    buffer.length);
                serverSocket.receive(pktIn);
                String response = new String(
                    pktIn.getData(),
                    0,
                    pktIn.getLength());
                System.out.println("SERVER : " + response);
            }

            System.out.println("Closing connection");
            serverSocket.close();
            userIn.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
    }
}
