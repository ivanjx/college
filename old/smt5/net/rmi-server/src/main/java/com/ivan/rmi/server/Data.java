package com.ivan.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.ivan.rmi.contract.DataInterface;

public class Data extends UnicastRemoteObject implements DataInterface
{

    protected Data() throws RemoteException
    {
        super();
    }

    @Override
    public void metodeDua() throws RemoteException
    {
        System.out.println("Metode dua dijalankan");
    }

    @Override
    public void metodeSatu() throws RemoteException
    {
        System.out.println("Metode satu dijalankan");
    }
    
}
