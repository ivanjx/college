package com.ivan.rmi.server;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class App 
{
    public static void main( String[] args ) throws NotBoundException, IOException
    {
        Registry registry = LocateRegistry.createRegistry(1099);

        Data data = new Data();
        registry.rebind("data", data);

        System.out.println("Server berhasil dijalankan");
        System.in.read();
    }
}
