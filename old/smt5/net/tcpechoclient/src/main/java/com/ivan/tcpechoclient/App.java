package com.ivan.tcpechoclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class App 
{
    private static final int PORT = 1234;
    
    public static void main( String[] args )
    {
        try
        {
            InetAddress serverAddress = InetAddress.getLocalHost();
            Socket serverLink = new Socket(serverAddress, PORT);
            BufferedReader serverIn = new BufferedReader(
                new InputStreamReader(
                    serverLink.getInputStream()));
            PrintWriter serverOut = new PrintWriter(
                serverLink.getOutputStream(), true);
            Scanner userIn = new Scanner(System.in);

            while (true)
            {
                System.out.print("Enter message: ");
                String message = userIn.nextLine();
                serverOut.println(message);
                String response = serverIn.readLine();
                System.out.println("SERVER " + response);

                if (message.equals("close"))
                {
                    break;
                }
            }

            System.out.println("Closing connection");
            userIn.close();
            serverLink.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
    }
}
