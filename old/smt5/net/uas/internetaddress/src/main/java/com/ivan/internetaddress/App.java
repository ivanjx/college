package com.ivan.internetaddress;

import java.net.InetAddress;

public class App {
    public static void main( String[] args ) {
        try {
            InetAddress address = InetAddress.getByName("istn.ac.id");
            System.out.println(address);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
