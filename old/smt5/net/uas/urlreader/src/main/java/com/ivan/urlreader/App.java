package com.ivan.urlreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class App {
    public static void main( String[] args ) throws IOException {
        URL url = new URL("https://google.co.id");
        URLConnection connection = url.openConnection();
        BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                connection.getInputStream()
            )
        );
        
        System.out.println("OUTPUT:");

        while (true) {
            String inputLine = reader.readLine();

            if (inputLine == null) {
                break;
            }

            System.out.println(inputLine);
        }

        reader.close();
    }
}
