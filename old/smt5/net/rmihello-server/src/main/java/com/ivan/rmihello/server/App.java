package com.ivan.rmihello.server;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class App 
{
    public static void main( String[] args ) throws IOException
    {
        Registry registry = LocateRegistry.createRegistry(1099);
        SayHelloServer sayHello = new SayHelloServer();
        registry.rebind("sayHello", sayHello);

        System.out.println("Server telah berjalan");
        System.in.read();
    }
}
