package com.ivan.rmihello.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.ivan.rmihello.contract.SayHelloInterface;

public class SayHelloServer extends UnicastRemoteObject implements SayHelloInterface
{

    protected SayHelloServer() throws RemoteException
    {
        super();
    }

    @Override
    public String sayHello(String nama) throws RemoteException
    {
        return "Hello " + nama;
    }
    
}
