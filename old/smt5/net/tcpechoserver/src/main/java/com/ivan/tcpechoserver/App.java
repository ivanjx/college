package com.ivan.tcpechoserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class App 
{
    private static final int PORT = 1234;

    private static void run(Socket link) throws IOException
    {
        System.out.println("Accepted connection from " + link.getRemoteSocketAddress());
        BufferedReader in = new BufferedReader(
            new InputStreamReader(
                link.getInputStream()));
        PrintWriter out = new PrintWriter(
            link.getOutputStream(),
            true);
        
        int numMessages = 0;
        
        while (true)
        {
            String message = in.readLine();
            ++numMessages;

            if (message.equals("close"))
            {
                break;
            }

            out.println("Message " + numMessages + " : " + message);
        }

        out.println(numMessages + " message(s) received.");
        System.out.println("Connection closed from " + link.getRemoteSocketAddress());
    }

    public static void main( String[] args ) throws Exception
    {
        System.out.println("Opening port " + PORT + " ...");
        ServerSocket server = new ServerSocket(PORT);

        while (true)
        {
            try
            {
                run(server.accept());
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
        }
    }
}
