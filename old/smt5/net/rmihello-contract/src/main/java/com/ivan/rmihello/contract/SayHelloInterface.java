package com.ivan.rmihello.contract;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SayHelloInterface extends Remote
{
    public String sayHello(String nama) throws RemoteException;
}
