package com.ivan.threadjoin;

public class ThreadJoinPrintName implements Runnable 
{
    Thread _thread;

    public ThreadJoinPrintName(String name)
    {
        _thread = new Thread(this, name);
        _thread.start();
    }

    @Override
    public void run() 
    {
        String name = _thread.getName();

        for (int i = 0; i < 20; ++i)
        {
            System.out.print(name);

            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex) { }
        }
    }
}
