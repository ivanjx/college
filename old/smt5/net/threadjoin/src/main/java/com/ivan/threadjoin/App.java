package com.ivan.threadjoin;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Threads will be running...");

        ThreadJoinPrintName p1 = new ThreadJoinPrintName("A");
        ThreadJoinPrintName p2 = new ThreadJoinPrintName("B");
        ThreadJoinPrintName p3 = new ThreadJoinPrintName("C");
        ThreadJoinPrintName p4 = new ThreadJoinPrintName("D");

        try
        {
            p1._thread.join();
            p2._thread.join();
            p3._thread.join();
            p4._thread.join();
        }
        catch (InterruptedException ex) { }

        System.out.println("Threads killed.");
    }
}
