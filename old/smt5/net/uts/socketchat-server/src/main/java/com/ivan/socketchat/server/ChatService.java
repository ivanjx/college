package com.ivan.socketchat.server;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public interface ChatService {
    public User[] getUsers() throws Exception;
    public void registerUserConnectedListener(UserConnectedListener listener);
    public void unregisterUserConnectedListener(UserConnectedListener listener);
    public void connectUser(User user) throws Exception;
    public void registerUserDisconnectedListener(UserDisconnectedListener listener);
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener);
    public void disconnectUser(User user) throws Exception;
    public void registerNewMessageListener(NewMessageListener listener);
    public void unregisterNewMessageListener(NewMessageListener listener);
    public Message[] getMessages() throws Exception;
    public Message addNewMessage(Message message) throws Exception;
}
