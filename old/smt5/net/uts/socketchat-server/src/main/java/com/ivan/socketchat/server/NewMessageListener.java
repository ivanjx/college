package com.ivan.socketchat.server;

import java.util.EventListener;

import com.ivan.socketchat.contract.Message;

public interface NewMessageListener extends EventListener {

    public void onNewMessage(Message message);
    
}
