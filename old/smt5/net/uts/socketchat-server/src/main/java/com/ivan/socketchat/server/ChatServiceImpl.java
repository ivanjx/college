package com.ivan.socketchat.server;

import java.time.LocalDateTime;
import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class ChatServiceImpl implements ChatService {

    private int _idCounter;
    private ArrayList<UserConnectedListener> _userConnectedListeners;
    private ArrayList<UserDisconnectedListener> _userDisconnectedListeners;
    private ArrayList<NewMessageListener> _newMessageListeners;
    private ArrayList<User> _users;
    private ArrayList<Message> _messages;

    protected ChatServiceImpl() {
        super();

        _userConnectedListeners = new ArrayList<UserConnectedListener>();
        _userDisconnectedListeners = new ArrayList<UserDisconnectedListener>();
        _newMessageListeners = new ArrayList<NewMessageListener>();
        _users = new ArrayList<User>();
        _messages = new ArrayList<Message>();
    }

    @Override
    public User[] getUsers() throws Exception {
        synchronized(_users) {
            return _users.toArray(new User[0]);
        }
    }

    @Override
    public void registerUserConnectedListener(UserConnectedListener listener) {
        _userConnectedListeners.add(listener);
    }

    @Override
    public void unregisterUserConnectedListener(UserConnectedListener listener) {
        _userConnectedListeners.remove(listener);
    }

    void broadcastUserConnected(User user) {
        for (UserConnectedListener listener : _userConnectedListeners) {
            listener.onUserConnected(user);
        }
    }

    @Override
    public void connectUser(User user) throws Exception {
        synchronized(_users) {
            if (_users.contains(user)) {
                throw new Exception("Username already taken");
            }
    
            _users.add(user);
        }

        broadcastUserConnected(user);
    }

    @Override
    public void registerUserDisconnectedListener(UserDisconnectedListener listener) {
        _userDisconnectedListeners.add(listener);
    }

    @Override
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener) {
        _userDisconnectedListeners.remove(listener);
    }

    void broadcastUserDisconnected(User user) {
        for (UserDisconnectedListener listener : _userDisconnectedListeners) {
            listener.onUserDisconnected(user);
        }
    }

    @Override
    public void disconnectUser(User user) throws Exception {
        synchronized(_users) {
            if (!_users.contains(user)) {
                throw new Exception("User already disconnected");
            }
    
            _users.remove(user);
        }

        broadcastUserDisconnected(user);
    }

    @Override
    public void registerNewMessageListener(NewMessageListener listener) {
        _newMessageListeners.add(listener);
    }

    @Override
    public void unregisterNewMessageListener(NewMessageListener listener) {
        _newMessageListeners.remove(listener);
    }

    void broadcastNewMessage(Message message) {
        for (NewMessageListener listener : _newMessageListeners) {
            listener.onNewMessage(message);
        }
    }

    @Override
    public Message[] getMessages() throws Exception {
        synchronized(_messages) {
            return _messages.toArray(new Message[0]);
        }
    }

    @Override
    public Message addNewMessage(Message message) throws Exception {
        synchronized(_users) {
            if (!_users.contains(message.getSender())) {
                throw new Exception("Unknown user");
            }
            
            synchronized(_messages) {
                if (message.getMessage() == null ||
                    message.getMessage().equals("")
                ) {
                    return null;
                }
    
                message.setId(++_idCounter);
                message.setTime(LocalDateTime.now());
                _messages.add(message);
            }
        }
        
        broadcastNewMessage(message);
        return message;
    }
    
}
