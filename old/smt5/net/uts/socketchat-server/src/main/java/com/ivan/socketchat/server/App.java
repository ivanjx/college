package com.ivan.socketchat.server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class App {
    private static ServerSocket _socketServer;
    private static Scanner _userIn;
    private static ChatServiceImpl _chatService;

    public static void main(String[] args) {
        try {
            // Prompting port.
            System.out.print("Listen on port: ");
            _userIn = new Scanner(System.in);
            int serverPort = _userIn.nextInt();

            if (serverPort < 0) {
                throw new Exception("Invalid port");
            }

            // Init chat service.
            _chatService = new ChatServiceImpl();

            // Start listening.
            System.out.println("Listening on port " + serverPort + "...");
            _socketServer = new ServerSocket(serverPort);

            while (true) {
                try {
                    Socket clientSocket = _socketServer.accept();
                    ClientConnectionImpl conn = new ClientConnectionImpl(clientSocket);
                    System.out.println("Received connection from " + conn.getName() + ".");

                    ClientHandler handler = new ClientHandler(_chatService, conn);
                    new Thread(handler).start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
