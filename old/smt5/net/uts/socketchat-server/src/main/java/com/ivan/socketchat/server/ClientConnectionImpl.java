package com.ivan.socketchat.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientConnectionImpl implements ClientConnection {

    private static final int SOCKET_TIMEOUT = 10 * 1000;
    private Socket _sock;
    private ObjectOutputStream _writer;
    private ObjectInputStream _reader;

    public ClientConnectionImpl(Socket sock) throws IOException {
        _sock = sock;
        _sock.setSoTimeout(SOCKET_TIMEOUT);
        _writer = new ObjectOutputStream(_sock.getOutputStream());
        _reader = new ObjectInputStream(_sock.getInputStream());
    }

    @Override
    public void writeObject(Object object) throws Exception {
        _writer.writeObject(object);
    }

    @Override
    public Object readObject() throws Exception {
        return _reader.readObject();
    }

    @Override
    public void close() throws Exception {
        _writer.close();
        _reader.close();
        _sock.close();
    }

    @Override
    public String getName() {
        return _sock.getRemoteSocketAddress().toString();
    }
    
}
