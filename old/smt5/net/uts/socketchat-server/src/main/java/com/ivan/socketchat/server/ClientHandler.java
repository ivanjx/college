package com.ivan.socketchat.server;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class ClientHandler implements Runnable, UserConnectedListener, UserDisconnectedListener, NewMessageListener {

    ChatService _service;
    ClientConnection _conn;
    User _user;

    public ClientHandler(
        ChatService service,
        ClientConnection conn
    ) {
        _service = service;
        _conn = conn;
    }

    void registerEvents() {
        _service.registerUserConnectedListener(this);
        _service.registerUserDisconnectedListener(this);
        _service.registerNewMessageListener(this);
    }

    void unregisterEvents() {
        _service.unregisterUserConnectedListener(this);
        _service.unregisterUserDisconnectedListener(this);
        _service.unregisterNewMessageListener(this);
    }

    void sendInvalidProto() throws Exception {
        synchronized(_conn) {
            _conn.writeObject("INVALID_PROTO");
        }
    }

    String getUserCommand() throws Exception {
        Object userReq = _conn.readObject();

        if (!(userReq instanceof String)) {
            return "";
        }

        return (String)userReq;
    }

    void handlePing() throws Exception {
        synchronized(_conn) {
            _conn.writeObject("OK");
        }
    }

    void handleGetMessages() throws Exception {
        if (_user == null) {
            sendInvalidProto();
            return;
        }

        synchronized(_conn) {
            _conn.writeObject(_service.getMessages());
        }
    }

    void handleGetUsers() throws Exception {
        if (_user == null) {
            sendInvalidProto();
            return;
        }

        synchronized(_conn) {
            _conn.writeObject(_service.getUsers());
        }
    }

    void handleSetIdentity() throws Exception {
        if (_user != null) {
            sendInvalidProto();
            return;
        }

        Object userReq = _conn.readObject();

        if (userReq instanceof User) {
            _user = (User)userReq;
            _service.connectUser(_user);
            return;
        }

        sendInvalidProto();
    }

    void handleAddMessage() throws Exception {
        if (_user == null) {
            sendInvalidProto();
            return;
        }

        Object userReq = _conn.readObject();

        if (!(userReq instanceof Message)) {
            sendInvalidProto();
            return;
        }

        Message message = (Message)userReq;
        message.setSender(_user);
        message = _service.addNewMessage(message);
        
        if (message == null) {
            sendInvalidProto();
        } else {
            synchronized(_conn) {
                _conn.writeObject(message);
            }
        }
    }

    void handleDisconnect() throws Exception {
        if (_user == null) {
            sendInvalidProto();
            return;
        }

        _service.disconnectUser(_user);
        _user = null;
        _conn.close();
        _conn = null;
        throw new Exception("Connection closed"); // Will exit main loop.
    }

    @Override
    public void run() {
        try {
            registerEvents();

            while (true) {
                String cmd = getUserCommand();

                if (cmd.equals("PING")) {
                    handlePing();
                } else if (cmd.equals("GET_MESSAGES")) {
                    handleGetMessages();
                } else if (cmd.equals("GET_USERS")) {
                    handleGetUsers();
                } else if (cmd.equals("SET_IDENTITY")) {
                    handleSetIdentity();
                } else if (cmd.equals("ADD_MESSAGE")) {
                    handleAddMessage();
                } else if (cmd.equals("DISCONNECT")) {
                    handleDisconnect();
                } else {
                    sendInvalidProto();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Unregister events.
            unregisterEvents();
            
            // Disconnect user.
            if (_user != null) {
                try {
                    _service.disconnectUser(_user);
                } catch (Exception e) { }
                _user = null;
            }

            // Close connection.
            if (_conn != null) {
                try {
                    _conn.close();
                } catch (Exception e) { }
            }
        }
    }

    @Override
    public void onNewMessage(Message message) {
        if (_user == null ||
            message.getSender().equals(_user)
        ) {
            return;
        }

        synchronized(_conn) {
            try {
                _conn.writeObject("NEW_MESSAGE");
                _conn.writeObject(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUserDisconnected(User user) {
        if (_user == null ||
            user.equals(_user)
        ) {
            return;
        }

        synchronized(_conn) {
            try {
                _conn.writeObject("USER_DISCONNECT");
                _conn.writeObject(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUserConnected(User user) {
        if (_user == null ||
            user.equals(_user)
        ) {
            return;
        }

        synchronized(_conn) {
            try {
                _conn.writeObject("USER_CONNECT");
                _conn.writeObject(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
}
