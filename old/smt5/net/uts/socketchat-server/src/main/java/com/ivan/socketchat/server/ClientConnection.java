package com.ivan.socketchat.server;

public interface ClientConnection {
    public String getName();
    public void writeObject(Object object) throws Exception;
    public Object readObject() throws Exception;
    public void close() throws Exception;
}
