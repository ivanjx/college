package com.ivan.socketchat.server;

import java.util.EventListener;

import com.ivan.socketchat.contract.User;

public interface UserDisconnectedListener extends EventListener {
    
    public void onUserDisconnected(User user);
    
}
