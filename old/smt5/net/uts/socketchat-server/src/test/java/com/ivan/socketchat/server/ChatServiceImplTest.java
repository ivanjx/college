package com.ivan.socketchat.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

import org.junit.Test;

public class ChatServiceImplTest {
    ChatServiceImpl _service;

    public ChatServiceImplTest() {
        _service = new ChatServiceImpl();
    }

    @Test
    public void connectUser_test() throws Exception {
        // Listener.
        UserConnectedListenerTestImpl listener = new UserConnectedListenerTestImpl();
        _service.registerUserConnectedListener(listener);

        // Test.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Assert.
        User[] users = _service.getUsers();
        assertNotNull(users);
        assertEquals(2, users.length);
        assertEquals(user, users[0]);
        assertEquals(user2, users[1]);
        assertEquals(2, listener.users.size());
        assertEquals(user, listener.users.get(0));
        assertEquals(user2, listener.users.get(1));
    }

    @Test
    public void connectUser_preventDuplicateUsername() throws Exception {
        // Listener.
        UserConnectedListenerTestImpl listener = new UserConnectedListenerTestImpl();
        _service.registerUserConnectedListener(listener);

        // Test.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);
        
        User user2 = new User();
        user2.setUsername("ivan");
        Exception exception = null;

        try {
            _service.connectUser(user2);
        } catch (Exception ex) {
            exception = ex;
        }

        // Assert.
        assertNotNull(exception);
        assertEquals("Username already taken", exception.getMessage());
        User[] users = _service.getUsers();
        assertEquals(1, users.length);
        assertEquals(user, users[0]);
        assertEquals(1, listener.users.size());
        assertEquals(user, listener.users.get(0));
    }

    @Test
    public void connectUser_unregisterListener() throws Exception {
        // Listener.
        UserConnectedListenerTestImpl listener = new UserConnectedListenerTestImpl();
        _service.registerUserConnectedListener(listener);

        // Listening.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        // Test.
        _service.unregisterUserConnectedListener(listener);
        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Assert.
        assertEquals(1, listener.users.size());
        assertEquals(user, listener.users.get(0));
    }

    @Test
    public void disconnectUser_test() throws Exception {
        // Sample users.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Listener.
        UserDisconnectedListenerTestImpl listener = new UserDisconnectedListenerTestImpl();
        _service.registerUserDisconnectedListener(listener);

        // Test.
        _service.disconnectUser(user);

        // Assert.
        User[] users = _service.getUsers();
        assertNotNull(users);
        assertEquals(1, users.length);
        assertEquals(user2, users[0]);
        assertEquals(1, listener.users.size());
        assertEquals(user, listener.users.get(0));
    }

    @Test
    public void disconnectUser_preventDuplicateCalls() throws Exception {
        // Sample users.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Listener.
        UserDisconnectedListenerTestImpl listener = new UserDisconnectedListenerTestImpl();
        _service.registerUserDisconnectedListener(listener);

        // Test.
        _service.disconnectUser(user);
        _service.disconnectUser(user2);
        Exception exception = null;

        try {
            _service.disconnectUser(user);
        } catch (Exception ex) {
            exception = ex;
        }

        // Assert.
        assertNotNull(exception);
        assertEquals("User already disconnected", exception.getMessage());
        assertEquals(0, _service.getUsers().length);
        assertEquals(2, listener.users.size());
        assertEquals(user, listener.users.get(0));
        assertEquals(user2, listener.users.get(1));
    }

    @Test
    public void disconnectUser_unregisterListener() throws Exception {
        // Sample users.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Listener.
        UserDisconnectedListenerTestImpl listener = new UserDisconnectedListenerTestImpl();
        _service.registerUserDisconnectedListener(listener);

        // Listening.
        _service.disconnectUser(user);

        // Test.
        _service.unregisterUserDisconnectedListener(listener);
        _service.disconnectUser(user2);

        // Assert.
        assertEquals(1, listener.users.size());
        assertEquals(user, listener.users.get(0));
    }

    @Test
    public void addNewMessage2_test() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        // Listener.
        NewMessageListenerTestImpl listener = new NewMessageListenerTestImpl();
        _service.registerNewMessageListener(listener);

        // Test.
        Message message = new Message();
        message.setMessage("hello");
        message.setSender(user);
        Message result = _service.addNewMessage(message);

        // Assert.
        assertEquals(1, result.getId());
        assertEquals(user, result.getSender());
        assertEquals("hello", result.getMessage());
        assertTrue(LocalDateTime.now().compareTo(result.getTime()) >= 0);
        assertTrue(LocalDateTime.now().compareTo(result.getTime()) < 5);
        Message[] messages = _service.getMessages();
        assertEquals(1, messages.length);
        assertEquals(result, messages[0]);
        assertEquals(1, listener.messages.size());
        assertEquals(result, listener.messages.get(0));
    }

    @Test
    public void addNewMessage_rejectUnknownUser() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Listener.
        NewMessageListenerTestImpl listener = new NewMessageListenerTestImpl();
        _service.registerNewMessageListener(listener);

        // Test.
        Message message = new Message();
        message.setMessage("hello");
        message.setSender(user);
        Exception exception = null;

        try {
            _service.addNewMessage(message);
        } catch (Exception ex) {
            exception = ex;
        }

        // Assert.
        assertNotNull(exception);
        assertEquals("Unknown user", exception.getMessage());
        assertEquals(0, _service.getMessages().length);
        assertEquals(0, listener.messages.size());
    }

    @Test
    public void addNewMessage2_ignoreEmptyMessage() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        // Listener.
        NewMessageListenerTestImpl listener = new NewMessageListenerTestImpl();
        _service.registerNewMessageListener(listener);

        // Test.
        Message message = new Message();
        message.setMessage("");
        message.setSender(user);
        Message result = _service.addNewMessage(message);

        // Assert.
        assertNull(result);
        assertEquals(0, _service.getMessages().length);
        assertEquals(0, listener.messages.size());
    }

    @Test
    public void addNewMessage_correctIdAssignment() throws Exception {
        // Sample users.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        User user2 = new User();
        user2.setUsername("john");
        _service.connectUser(user2);

        // Listener.
        NewMessageListenerTestImpl listener = new NewMessageListenerTestImpl();
        _service.registerNewMessageListener(listener);

        // Test.
        Message message = new Message();
        message.setMessage("1");
        message.setSender(user);
        Message result = _service.addNewMessage(message);

        message = new Message();
        message.setMessage("2");
        message.setSender(user2);
        Message result2 = _service.addNewMessage(message);

        message = new Message();
        message.setMessage("3");
        message.setSender(user);
        Message result3 = _service.addNewMessage(message);

        // Assert.
        assertEquals(1, result.getId());
        assertEquals("1", result.getMessage());
        assertEquals(2, result2.getId());
        assertEquals("2", result2.getMessage());
        assertEquals(3, result3.getId());
        assertEquals("3", result3.getMessage());

        assertEquals(3, listener.messages.size());
        assertEquals(result, listener.messages.get(0));
        assertEquals(result2, listener.messages.get(1));
        assertEquals(result3, listener.messages.get(2));

        Message[] messages = _service.getMessages();
        assertEquals(result, messages[0]);
        assertEquals(result2, messages[1]);
        assertEquals(result3, messages[2]);
    }

    @Test
    public void addNewMessage_unregisterNewMessageListener() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");
        _service.connectUser(user);

        // Listener.
        NewMessageListenerTestImpl listener = new NewMessageListenerTestImpl();
        _service.registerNewMessageListener(listener);

        // Listening to messages.
        Message message = new Message();
        message.setMessage("1");
        message.setSender(user);
        _service.addNewMessage(message);

        // Test.
        _service.unregisterNewMessageListener(listener);
        message = new Message();
        message.setMessage("2");
        message.setSender(user);
        _service.addNewMessage(message);

        // Assert.
        assertEquals(2, _service.getMessages().length);
        assertEquals(1, listener.messages.size());
        assertEquals("1", listener.messages.get(0).getMessage());
    }
}
