package com.ivan.socketchat.server;

import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;

public class NewMessageListenerTestImpl implements NewMessageListener {

    public ArrayList<Message> messages;

    public NewMessageListenerTestImpl() {
        messages = new ArrayList<Message>();
    }

    @Override
    public void onNewMessage(Message message) {
        messages.add(message);
    }
    
}
