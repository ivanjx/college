package com.ivan.socketchat.server;

import java.util.ArrayList;

import com.ivan.socketchat.contract.User;

public class UserConnectedListenerTestImpl implements UserConnectedListener {

    public ArrayList<User> users;

    public UserConnectedListenerTestImpl() {
        users = new ArrayList<User>();
    }

    @Override
    public void onUserConnected(User user) {
        users.add(user);
    }
    
}
