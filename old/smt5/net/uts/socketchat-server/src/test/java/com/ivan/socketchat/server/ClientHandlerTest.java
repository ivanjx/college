package com.ivan.socketchat.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

import org.junit.Test;

public class ClientHandlerTest {
    FakeChatServiceImpl _chatService;
    FakeClientConnectionImpl _clientConnection;
    ClientHandler _handler;

    public ClientHandlerTest() {
        _chatService = new FakeChatServiceImpl();
        _clientConnection = new FakeClientConnectionImpl();
        _handler = new ClientHandler(
            _chatService,
            _clientConnection
        );
    }

    @Test
    public void run_registerEvents() throws Exception {
        // Test.
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(1, _chatService.registerUserConnectedListenerArgs.size());
        assertEquals(_handler, _chatService.registerUserConnectedListenerArgs.get(0));
        assertEquals(1, _chatService.registerUserDisconnectedListenerArgs.size());
        assertEquals(_handler, _chatService.registerUserDisconnectedListenerArgs.get(0));
        assertEquals(1, _chatService.registerNewMessageListenerArgs.size());
        assertEquals(_handler, _chatService.registerNewMessageListenerArgs.get(0));

        assertEquals(0, _chatService.unregisterUserConnectedListenerArgs.size());
        assertEquals(0, _chatService.unregisterUserDisconnectedListenerArgs.size());
        assertEquals(0, _chatService.unregisterNewMessageListenerArgs.size());
    }

    @Test
    public void run_unregisterEventsAfterClose() throws Exception {
        // Test.
        _handler.run();

        // Assert.
        assertEquals(1, _chatService.unregisterUserConnectedListenerArgs.size());
        assertEquals(_handler, _chatService.unregisterUserConnectedListenerArgs.get(0));
        assertEquals(1, _chatService.unregisterUserDisconnectedListenerArgs.size());
        assertEquals(_handler, _chatService.unregisterUserDisconnectedListenerArgs.get(0));
        assertEquals(1, _chatService.unregisterNewMessageListenerArgs.size());
        assertEquals(_handler, _chatService.unregisterNewMessageListenerArgs.get(0));
    }

    @Test
    public void run_disconnectUserAfterClose() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.readObjects.add("PING");
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("OK", _clientConnection.writeObjects.get(0));
        assertEquals(1, _chatService.disconnectUserArgs.size());
        assertEquals(user, _chatService.disconnectUserArgs.get(0));
    }

    @Test
    public void run_closeConnectionAfterDone() throws Exception {
        // Test.
        _clientConnection.readObjects.add("PING");
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("OK", _clientConnection.writeObjects.get(0));
        assertEquals(1, _clientConnection.closeCallCount);
    }

    @Test
    public void run_handlePing() throws Exception {
        // Ping requests.
        _clientConnection.readObjects.add("PING");
        _clientConnection.readObjects.add("PING");
        _clientConnection.readObjects.add("PING");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(3, _clientConnection.writeObjects.size());
        assertEquals("OK", _clientConnection.writeObjects.get(0));
        assertEquals("OK", _clientConnection.writeObjects.get(1));
        assertEquals("OK", _clientConnection.writeObjects.get(2));
    }

    @Test
    public void run_handleGetMessages() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Sample messages.
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello1");
        _chatService.addNewMessage(message);

        Message message2 = new Message();
        message2.setId(2);
        message2.setSender(user);
        message2.setTime(LocalDateTime.now());
        message2.setMessage("hello2");
        _chatService.addNewMessage(message2);

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Get messages request.
        _clientConnection.readObjects.add("GET_MESSAGES");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertTrue(_clientConnection.writeObjects.get(0) instanceof Message[]);
        Message[] result = (Message[])_clientConnection.writeObjects.get(0);
        assertEquals(2, result.length);
        assertEquals(message, result[0]);
        assertEquals(message2, result[1]);
    }

    @Test
    public void run_handleGetMessages_handleNoIdentity() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Sample messages.
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello1");
        _chatService.addNewMessage(message);

        // Get messages request.
        _clientConnection.readObjects.add("GET_MESSAGES");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0));
    }

    @Test
    public void run_handleGetUsers() throws Exception {
        // Sample users.
        _chatService.getUsersResult = new User[2];
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUsersResult[0] = user;

        User user2 = new User();
        user2.setUsername("john");
        _chatService.getUsersResult[1] = user2;

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Get users request.
        _clientConnection.readObjects.add("GET_USERS");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertTrue(_clientConnection.writeObjects.get(0) instanceof User[]);
        User[] result = (User[])_clientConnection.writeObjects.get(0);
        assertEquals(2, result.length);
        assertEquals(user, result[0]);
        assertEquals(user2, result[1]);
    }

    @Test
    public void run_handleGetUsers_handleNoIdentity() throws Exception {
        // Sample users.
        _chatService.getUsersResult = new User[1];
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUsersResult[0] = user;

        // Get users request.
        _clientConnection.readObjects.add("GET_USERS");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0));
    }

    @Test
    public void run_handleInvalidUserRequest() throws Exception {
        // Sample client requests.
        _clientConnection.readObjects.add(10);
        _clientConnection.readObjects.add("WHATEVER");
        _clientConnection.readObjects.add("PING");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(3, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0));
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(1));
        assertEquals("OK", _clientConnection.writeObjects.get(2));
    }

    @Test
    public void run_handleSetIdentity() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(0, _clientConnection.writeObjects.size());
        assertEquals(1, _chatService.connectUserArgs.size());
        assertEquals(user, _chatService.connectUserArgs.get(0));
    }

    @Test
    public void run_handleSetIdentity_handleInvalidRequest() throws Exception {
        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add("USER");

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0));
        assertEquals(0, _chatService.connectUserArgs.size());
    }

    @Test
    public void run_handleSetIdentity_rejectMultipleRequests() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;

        // Test.
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0)); // For SET_IDENTITY.
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(1)); // For getUserCommand.
        assertEquals(1, _chatService.connectUserArgs.size());
        assertEquals(user, _chatService.connectUserArgs.get(0));
        assertEquals(0, _chatService.disconnectUserArgs.size());
    }

    @Test
    public void run_handleDisconnect() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Disconnect request.
        _clientConnection.readObjects.add("DISCONNECT");

        // Dummy for test requests.
        _clientConnection.readObjects.add("PING");
        _clientConnection.blockReadObjectAfterNoRead = true;

        // Test.
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(1, _clientConnection.readObjects.size());
        assertEquals("PING", _clientConnection.readObjects.get(0));
        assertEquals(1, _clientConnection.closeCallCount);
        assertEquals(1, _chatService.disconnectUserArgs.size());
        assertEquals(user, _chatService.disconnectUserArgs.get(0));
    }

    @Test
    public void run_handleDisconnect_handleNoUserIdentity() throws Exception {
        // Disconnect request.
        _clientConnection.readObjects.add("DISCONNECT");

        // Dummy for test requests.
        _clientConnection.readObjects.add("PING");
        _clientConnection.blockReadObjectAfterNoRead = true;

        // Test.
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _clientConnection.closeCallCount);
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0)); // For DISCONNECT.
        assertEquals("OK", _clientConnection.writeObjects.get(1)); // For getUserCommand.
    }

    @Test
    public void run_handleAddMessage() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Add message requests.
        _clientConnection.readObjects.add("ADD_MESSAGE");
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setMessage("hello");
        message.setTime(LocalDateTime.now());
        _clientConnection.readObjects.add(message);

        _clientConnection.readObjects.add("ADD_MESSAGE");
        message = new Message();
        message.setId(2);
        message.setSender(null);
        message.setMessage("hello 2");
        message.setTime(LocalDateTime.now());
        _clientConnection.readObjects.add(message);

        // Test.
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(2, _clientConnection.writeObjects.size());
        
        Message resp = (Message)_clientConnection.writeObjects.get(0);
        assertEquals(1, resp.getId());
        assertEquals(user, resp.getSender());
        assertEquals("hello", resp.getMessage());

        Message resp2 = (Message)_clientConnection.writeObjects.get(1);
        assertEquals(2, resp2.getId());
        assertEquals(user, resp2.getSender());
        assertEquals("hello 2", resp2.getMessage());

        assertEquals(2, _chatService.messages.size());
        assertEquals(resp, _chatService.messages.get(0));
        assertEquals(resp2, _chatService.messages.get(1));
    }

    @Test
    public void run_handleAddMessage_handleNoUserIdentity() throws Exception {
        // Add message requests.
        _clientConnection.readObjects.add("ADD_MESSAGE");
        Message message = new Message();
        message.setMessage("hello");
        _clientConnection.readObjects.add(message);
        _clientConnection.blockReadObjectAfterNoRead = true;

        // Test.
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _clientConnection.closeCallCount);
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0)); // For ADD_MESSAGE.
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(1)); // For getUserCommand.
        assertEquals(0, _chatService.messages.size());
    }

    @Test
    public void run_handleAddMessage_handleInvalidPayload() throws Exception {
        // Add message requests.
        _clientConnection.readObjects.add("ADD_MESSAGE");
        _clientConnection.readObjects.add("MESSAGE");
        _clientConnection.blockReadObjectAfterNoRead = true;

        // Test.
        new Thread(_handler).start();
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _clientConnection.closeCallCount);
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0)); // For ADD_MESSAGE.
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(1)); // For getUserCommand.
        assertEquals(0, _chatService.messages.size());
    }

    @Test
    public void run_handleAddMessage_handleNullMessageResult() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity requests.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);

        // Add message requests.
        _clientConnection.readObjects.add("ADD_MESSAGE");
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setMessage("hello");
        message.setTime(LocalDateTime.now());
        _clientConnection.readObjects.add(message);

        // Test
        _chatService.returnNullAddNewMessage = true;
        _handler.run();

        // Assert.
        assertEquals(0, _clientConnection.readObjects.size());
        assertEquals(1, _clientConnection.writeObjects.size());
        assertEquals("INVALID_PROTO", _clientConnection.writeObjects.get(0));
    }

    @Test
    public void onUserConnected_test() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        User user2 = new User();
        user2.setUsername("john");
        _handler.onUserConnected(user2);

        // Assert.
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("USER_CONNECT", _clientConnection.writeObjects.get(0));
        assertEquals(user2, _clientConnection.writeObjects.get(1));
    }

    @Test
    public void onUserConnected_ignoreSameIdentity() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        _handler.onUserConnected(user);

        // Assert.
        assertEquals(0, _clientConnection.writeObjects.size());
    }

    @Test
    public void onUserDisconnected_test() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        User user2 = new User();
        user2.setUsername("john");
        _handler.onUserDisconnected(user2);

        // Assert.
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("USER_DISCONNECT", _clientConnection.writeObjects.get(0));
        assertEquals(user2, _clientConnection.writeObjects.get(1));
    }

    @Test
    public void onUserDisconnected_ignoreSameIdentity() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        _handler.onUserDisconnected(user);

        // Assert.
        assertEquals(0, _clientConnection.writeObjects.size());
    }

    @Test
    public void onNewMessage_test() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        User user2 = new User();
        user2.setUsername("john");
        Message message = new Message();
        message.setId(1);
        message.setSender(user2);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        _handler.onNewMessage(message);

        // Assert.
        assertEquals(2, _clientConnection.writeObjects.size());
        assertEquals("NEW_MESSAGE", _clientConnection.writeObjects.get(0));
        assertEquals(message, _clientConnection.writeObjects.get(1));
    }

    @Test
    public void onNewMessage_ignoreSameIdentity() throws Exception {
        // Sample user.
        User user = new User();
        user.setUsername("ivan");

        // Set identity.
        _clientConnection.readObjects.add("SET_IDENTITY");
        _clientConnection.readObjects.add(user);
        _clientConnection.blockReadObjectAfterNoRead = true;
        new Thread(_handler).start();
        Thread.sleep(100);

        // Test.
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        _handler.onNewMessage(message);

        // Assert.
        assertEquals(0, _clientConnection.writeObjects.size());
    }
}
