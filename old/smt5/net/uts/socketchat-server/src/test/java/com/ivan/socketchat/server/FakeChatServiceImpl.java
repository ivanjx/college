package com.ivan.socketchat.server;

import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class FakeChatServiceImpl implements ChatService {

    public ArrayList<UserConnectedListener> registerUserConnectedListenerArgs;
    public ArrayList<UserDisconnectedListener> registerUserDisconnectedListenerArgs;
    public ArrayList<NewMessageListener> registerNewMessageListenerArgs;
    public ArrayList<UserConnectedListener> unregisterUserConnectedListenerArgs;
    public ArrayList<UserDisconnectedListener> unregisterUserDisconnectedListenerArgs;
    public ArrayList<NewMessageListener> unregisterNewMessageListenerArgs;
    public User[] getUsersResult;
    public ArrayList<User> connectUserArgs;
    public ArrayList<User> disconnectUserArgs;
    public ArrayList<Message> messages;
    public boolean returnNullAddNewMessage;
    public Exception connectUserException;
    public Exception disconnectUserException;
    public Exception addNewMessageException;

    public FakeChatServiceImpl() {
        registerUserConnectedListenerArgs = new ArrayList<>();
        registerUserDisconnectedListenerArgs = new ArrayList<>();
        registerNewMessageListenerArgs = new ArrayList<>();
        unregisterUserConnectedListenerArgs = new ArrayList<>();
        unregisterUserDisconnectedListenerArgs = new ArrayList<>();
        unregisterNewMessageListenerArgs = new ArrayList<>();
        connectUserArgs = new ArrayList<>();
        disconnectUserArgs = new ArrayList<>();
        messages = new ArrayList<>();
    }

    @Override
    public User[] getUsers() throws Exception {
        return getUsersResult;
    }

    @Override
    public void registerUserConnectedListener(UserConnectedListener listener) {
        registerUserConnectedListenerArgs.add(listener);
    }

    @Override
    public void unregisterUserConnectedListener(UserConnectedListener listener) {
        unregisterUserConnectedListenerArgs.add(listener);
    }

    @Override
    public void connectUser(User user) throws Exception {
        if (connectUserException != null) {
            throw connectUserException;
        }

        connectUserArgs.add(user);
    }

    @Override
    public void registerUserDisconnectedListener(UserDisconnectedListener listener) {
        registerUserDisconnectedListenerArgs.add(listener);
    }

    @Override
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener) {
        unregisterUserDisconnectedListenerArgs.add(listener);
    }

    @Override
    public void disconnectUser(User user) throws Exception {
        if (disconnectUserException != null) {
            throw disconnectUserException;
        }

        disconnectUserArgs.add(user);
    }

    @Override
    public void registerNewMessageListener(NewMessageListener listener) {
        registerNewMessageListenerArgs.add(listener);
    }

    @Override
    public void unregisterNewMessageListener(NewMessageListener listener) {
        unregisterNewMessageListenerArgs.add(listener);
    }

    @Override
    public Message[] getMessages() throws Exception {
        return messages.toArray(new Message[0]);
    }

    @Override
    public Message addNewMessage(Message message) throws Exception {
        if (addNewMessageException != null) {
            throw addNewMessageException;
        }

        messages.add(message);

        if (returnNullAddNewMessage) {
            return null;
        }

        return message;
    }
    
}
