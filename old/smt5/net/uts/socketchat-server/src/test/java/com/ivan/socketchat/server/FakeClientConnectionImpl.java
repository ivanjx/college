package com.ivan.socketchat.server;

import java.util.ArrayList;

public class FakeClientConnectionImpl implements ClientConnection {

    public boolean isClosed;
    public ArrayList<Object> writeObjects;
    public ArrayList<Object> readObjects;
    public int closeCallCount;
    public boolean blockReadObjectAfterNoRead;

    public FakeClientConnectionImpl() {
        writeObjects = new ArrayList<Object>();
        readObjects = new ArrayList<Object>();
    }

    @Override
    public void writeObject(Object object) throws Exception {
        writeObjects.add(object);
    }

    @Override
    public Object readObject() throws Exception {
        if (readObjects.size() == 0) {
            if (blockReadObjectAfterNoRead) {
                Thread.sleep(1000 * 1000);
            }
            
            throw new Exception("No more read object");
        }

        Object ret = readObjects.get(0);
        readObjects.remove(0);

        if (ret instanceof Exception) {
            throw (Exception)ret;
        }

        return ret;
    }

    @Override
    public void close() throws Exception {
        isClosed = true;
        ++closeCallCount;
    }

    @Override
    public String getName() {
        return "test";
    }
    
}
