package com.ivan.socketchat.server;

import java.util.ArrayList;

import com.ivan.socketchat.contract.User;

public class UserDisconnectedListenerTestImpl implements UserDisconnectedListener {

    public ArrayList<User> users;

    public UserDisconnectedListenerTestImpl() {
        users = new ArrayList<User>();
    }

    @Override
    public void onUserDisconnected(User user) {
        users.add(user);
    }
    
}
