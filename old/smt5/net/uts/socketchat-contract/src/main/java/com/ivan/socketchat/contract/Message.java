package com.ivan.socketchat.contract;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Message implements Serializable {
    private static final long serialVersionUID = 1234L;

    private int _id;
    private LocalDateTime _time;
    private User _sender;
    private String _message;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public LocalDateTime getTime() {
        return _time;
    }

    public void setTime(LocalDateTime time) {
        _time = time;
    }

    public User getSender() {
        return _sender;
    }

    public void setSender(User sender) {
        _sender = sender;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(String message) {
        _message = message;
    }

    @Override
    public boolean equals(Object obj) {
        Message message = (Message)obj;

        return
            getId() == message.getId() &&
            getTime().equals(message.getTime()) &&
            getSender().equals(message.getSender()) &&
            getMessage().equals(message.getMessage());
    }
}
