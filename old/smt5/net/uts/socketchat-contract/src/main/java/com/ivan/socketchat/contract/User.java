package com.ivan.socketchat.contract;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1235L;

    private String _username;

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }

    @Override
    public boolean equals(Object obj) {
        User client = (User)obj;
        return getUsername().equals(client.getUsername());
    }
}
