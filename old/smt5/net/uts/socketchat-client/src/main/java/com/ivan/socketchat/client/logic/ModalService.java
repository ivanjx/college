package com.ivan.socketchat.client.logic;

public interface ModalService {
    void showError(String title, String message);
}
