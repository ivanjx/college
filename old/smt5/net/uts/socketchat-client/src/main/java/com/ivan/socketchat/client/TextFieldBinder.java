package com.ivan.socketchat.client;

import java.lang.reflect.Method;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.ivan.socketchat.client.logic.PropertyChangedListener;
import com.ivan.socketchat.client.logic.ViewModelBase;

// Source: https://stackoverflow.com/questions/12337986/binding-of-jtext-fields-value-to-info-class/12340908
public class TextFieldBinder implements PropertyChangedListener, DocumentListener {
    private ViewModelBase viewModel;
    private JTextField txtField;
    private String fieldName;
    private Class<?> setterType;

    public TextFieldBinder(ViewModelBase model, JTextField txtField, String fieldName) {
        this.viewModel = model;
        this.txtField = txtField;
        this.fieldName = fieldName;
        this.setterType = String.class;

        bind();
    }

    public TextFieldBinder(ViewModelBase model, JTextField txtField, String fieldName, Class<?> setterType) {
        this.viewModel = model;
        this.txtField = txtField;
        this.fieldName = fieldName;
        this.setterType = setterType;

        bind();
    }

    private void bind() {
        txtField.getDocument().addDocumentListener(this);
        viewModel.registerPropertyChangedListener(this);
    }

    @Override
    public void onPropertyChanged(ViewModelBase sender, String name) {
        if (sender != viewModel ||
            !name.equals(fieldName)
        ) {
            return;
        }

        try {
            Method method = viewModel.getClass().getDeclaredMethod("get" + name);
            Object ret = method.invoke(viewModel);

            if (ret == null) {
                return;
            }

            String newVal;

            if (ret instanceof String) {
                newVal = (String)ret;
            } else if (ret instanceof Integer) {
                newVal = ((Integer)ret).toString();
            } else {
                throw new Exception("Type not supported");
            }

            if (newVal.equals(txtField.getText())) {
                return; // Prevent infinite event firing.
            }

            txtField.setText(newVal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) { set(e); }
    
    @Override
    public void removeUpdate(DocumentEvent e) { set(e); }

    @Override
    public void changedUpdate(DocumentEvent e) { set(e); }

    private void set(DocumentEvent e) {
        try {
            String text = e.getDocument().getText(
                    e.getDocument().getStartPosition().getOffset(),
                    e.getDocument().getEndPosition().getOffset() - 1);
            Method method = viewModel.getClass().getDeclaredMethod(
                "set" + fieldName,
                setterType
            );
            
            if (setterType == String.class) {
                method.invoke(viewModel, text);
            } else if (setterType == Integer.class) {
                method.invoke(viewModel, Integer.parseInt(text));
            } else {
                throw new Exception("Type not supported");
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
