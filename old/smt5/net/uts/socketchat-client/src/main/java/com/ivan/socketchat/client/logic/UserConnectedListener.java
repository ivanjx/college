package com.ivan.socketchat.client.logic;

import java.util.EventListener;

import com.ivan.socketchat.contract.User;

public interface UserConnectedListener extends EventListener {
    public void onUserConnected(User user);
}
