package com.ivan.socketchat.client.logic;

public interface LoginStatusChangedListener {
    public void onLoginStatusChanged(boolean isLoggedIn);
}
