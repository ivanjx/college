package com.ivan.socketchat.client.logic;

public interface ConnectionService {
    public void connect(String host, int port) throws Exception;
    public void writeObject(Object object) throws Exception;
    public Object readObject() throws Exception;
    public void close() throws Exception;
}
