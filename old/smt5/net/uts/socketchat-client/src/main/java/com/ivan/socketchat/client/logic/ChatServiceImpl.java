package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class ChatServiceImpl implements ChatService {

    private ConnectionService _conn;
    private final Object _receiveMonitor;
    private ArrayList<Object> _receiveBuffer;
    private User _user;
    private ArrayList<LoginStatusChangedListener> _loginStatusChangedListeners;
    private ArrayList<UserConnectedListener> _userConnectedListeners;
    private ArrayList<UserDisconnectedListener> _userDisconnectedListeners;
    private ArrayList<NewMessageReceivedListener> _newMessageReceivedListeners;

    public ChatServiceImpl(ConnectionService conn) {
        _conn = conn;
        _receiveBuffer = new ArrayList<Object>();
        _receiveMonitor = new Object();
        _loginStatusChangedListeners = new ArrayList<LoginStatusChangedListener>();
        _userConnectedListeners = new ArrayList<UserConnectedListener>();
        _userDisconnectedListeners = new ArrayList<UserDisconnectedListener>();
        _newMessageReceivedListeners = new ArrayList<NewMessageReceivedListener>();
    }

    private void handleUserConnected() {
        try {
            // Reading payload.
            Object payload = _conn.readObject();

            if (!(payload instanceof User)) {
                throw new Exception("Invalid payload type");
            }

            User param = (User)payload;

            // Broadcasting message.
            for (UserConnectedListener listener : _userConnectedListeners) {
                listener.onUserConnected(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleUserDisconnected() {
        try {
            // Reading payload.
            Object payload = _conn.readObject();

            if (!(payload instanceof User)) {
                throw new Exception("Invalid payload type");
            }

            User param = (User)payload;

            // Broadcasting message.
            for (UserDisconnectedListener listener : _userDisconnectedListeners) {
                listener.onUserDisconnected(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleNewMessageReceived() {
        try {
            // Reading payload.
            Object payload = _conn.readObject();

            if (!(payload instanceof Message)) {
                throw new Exception("Invalid payload type");
            }

            Message param = (Message)payload;

            // Broadcasting message.
            for (NewMessageReceivedListener listener : _newMessageReceivedListeners) {
                listener.onNewMessageReceived(param);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void receiveServerBuffer() {
        // Read responses from server as soon as they arrives and store it in the buffer,
        // repeatedly, until manually disconnected.
        try {
            while (_user != null) {
                Object resp = _conn.readObject();

                if (resp instanceof String) {
                    String respStr = (String)resp;

                    if (respStr.equals("USER_CONNECT")) {
                        handleUserConnected();
                        continue;
                    } else if (respStr.equals("USER_DISCONNECT")) {
                        handleUserDisconnected();
                        continue;
                    } else if (respStr.equals("NEW_MESSAGE")) {
                        handleNewMessageReceived();
                        continue;
                    }
                }

                // Add to read buffer.
                synchronized(_receiveBuffer) {
                    _receiveBuffer.add(resp);
                }

                // Allow other reader to receive the response.
                synchronized(_receiveMonitor) {
                    _receiveMonitor.notifyAll();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                disconnect();
            } catch (Exception ed) {
                ed.printStackTrace();
            }

            // One last receive.
            synchronized(_receiveMonitor) {
                _receiveMonitor.notifyAll();
            }
        }
    }

    private void startPing() {
        // To keep the connection open.
        try {
            while (_user != null) {
                Thread.sleep(2000);
                ping();
            }
        } catch (Exception e) { }
    }

    private void broadcastLoginStatusChanged(boolean isLoggedIn) {
        for (LoginStatusChangedListener listener : _loginStatusChangedListeners) {
            listener.onLoginStatusChanged(isLoggedIn);
        }
    }

    @Override
    public void connect(String host, int port, String username) throws Exception {
        if (_user != null) {
            throw new Exception("Already connected");
        }

        // Opening connection.
        _conn.connect(host, port);

        // Setting identity.
        User user = new User();
        user.setUsername(username);

        // Registering user.
        synchronized(_conn) {
            _conn.writeObject("SET_IDENTITY");
            _conn.writeObject(user);
        }
        
        // Set state.
        _user = user;

        // Start receiving.
        new Thread(new Runnable() {
            @Override
            public void run() {
                receiveServerBuffer();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                startPing();
            }
        }).start();

        // Notify.
        broadcastLoginStatusChanged(true);
    }

    @Override
    public void disconnect() throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        // Sending disconnect.
        try {
            synchronized(_conn) {
                _conn.writeObject("DISCONNECT");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set state.
        _user = null;

        // Notify.
        broadcastLoginStatusChanged(false);

        // Closing connection.
        try {
            _conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUser() throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        return _user;
    }

    @SuppressWarnings("unchecked") // Trust me bro.
    private <T> T readServerResponse(Class<T> type) throws Exception {
        while (true) {
            // Checking available buffer.
            synchronized(_receiveBuffer) {
                for (Object data : _receiveBuffer) {
                    if (type.isInstance(data)) {
                        // Got expected response.
                        _receiveBuffer.remove(data);
                        return (T)data; // Suppressed.
                    }
                }
            }

            if (_user == null) {
                // Disconnected.
                return null;
            }

            // Wait for another server response.
            synchronized(_receiveMonitor) {
                _receiveMonitor.wait();
            }
        }
    }

    @Override
    public void ping() throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        // Sending ping.
        synchronized(_conn) {
            _conn.writeObject("PING");
        }

        // Reading response.
        String resp = readServerResponse(String.class);

        if (resp == null ||
            !resp.equals("OK")
        ) {
            throw new Exception("Invalid ping response");
        }
    }

    @Override
    public User[] getUsers() throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        // Sending request.
        synchronized(_conn) {
            _conn.writeObject("GET_USERS");
        }

        // Reading response.
        User[] resp = readServerResponse(User[].class);

        if (resp == null) {
            throw new Exception("Invalid get users response");
        }

        return resp;
    }

    @Override
    public Message[] getMessages() throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        // Sending request.
        synchronized(_conn) {
            _conn.writeObject("GET_MESSAGES");
        }

        // Reading response.
        Message[] resp = readServerResponse(Message[].class);

        if (resp == null) {
            throw new Exception("Invalid get messages response");
        }

        return resp;
    }

    @Override
    public Message addMessage(Message message) throws Exception {
        if (_user == null) {
            throw new Exception("Not connected");
        }

        if (message.getMessage() == null ||
            message.getMessage().equals("")
        ) {
            throw new Exception("Invalid message");
        }

        // Setting parameter.
        message.setSender(_user);

        synchronized(_conn) {
            // Sending.
            _conn.writeObject("ADD_MESSAGE");
            _conn.writeObject(message);
        }

        // Reading response.
        Message resp = readServerResponse(Message.class);

        if (resp == null) {
            throw new Exception("Invalid add message response");
        }

        return resp;
    }

    @Override
    public void registerLoginStatusChangedListener(LoginStatusChangedListener listener) {
        _loginStatusChangedListeners.add(listener);
    }

    @Override
    public void unregisterLoginStatusChangedListener(LoginStatusChangedListener listener) {
        _loginStatusChangedListeners.remove(listener);
    }

    @Override
    public void registerUserConnectedListener(UserConnectedListener listener) {
        _userConnectedListeners.add(listener);
    }

    @Override
    public void registerUserDisconnectedListener(UserDisconnectedListener listener) {
        _userDisconnectedListeners.add(listener);
    }

    @Override
    public void registerNewMessageReceivedListener(NewMessageReceivedListener listener) {
        _newMessageReceivedListeners.add(listener);
    }

    @Override
    public void unregisterUserConnectedListener(UserConnectedListener listener) {
        _userConnectedListeners.remove(listener);
    }

    @Override
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener) {
        _userDisconnectedListeners.remove(listener);
    }

    @Override
    public void unregisterNewMessageReceivedListener(NewMessageReceivedListener listener) {
        _newMessageReceivedListeners.remove(listener);
    }
    
}
