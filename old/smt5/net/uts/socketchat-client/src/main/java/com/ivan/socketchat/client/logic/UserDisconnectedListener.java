package com.ivan.socketchat.client.logic;

import com.ivan.socketchat.contract.User;

public interface UserDisconnectedListener {
    public void onUserDisconnected(User user);
}
