package com.ivan.socketchat.client.logic;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public interface ChatService {
    public void connect(String host, int port, String username) throws Exception;
    public void disconnect() throws Exception;
    public User getUser() throws Exception;
    public void ping() throws Exception;
    public User[] getUsers() throws Exception;
    public Message[] getMessages() throws Exception;
    public Message addMessage(Message message) throws Exception;
    public void registerLoginStatusChangedListener(LoginStatusChangedListener listener);
    public void registerUserConnectedListener(UserConnectedListener listener);
    public void registerUserDisconnectedListener(UserDisconnectedListener listener);
    public void registerNewMessageReceivedListener(NewMessageReceivedListener listener);
    public void unregisterLoginStatusChangedListener(LoginStatusChangedListener listener);
    public void unregisterUserConnectedListener(UserConnectedListener listener);
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener);
    public void unregisterNewMessageReceivedListener(NewMessageReceivedListener listener);
}
