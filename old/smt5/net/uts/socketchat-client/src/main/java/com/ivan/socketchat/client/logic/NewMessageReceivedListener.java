package com.ivan.socketchat.client.logic;

import com.ivan.socketchat.contract.Message;

public interface NewMessageReceivedListener {
    public void onNewMessageReceived(Message message);
}
