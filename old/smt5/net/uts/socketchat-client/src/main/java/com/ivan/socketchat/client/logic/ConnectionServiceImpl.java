package com.ivan.socketchat.client.logic;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ConnectionServiceImpl implements ConnectionService {

    private static final int SOCKET_TIMEOUT = 10 * 1000;

    private Socket _sock;
    private ObjectOutputStream _writer;
    private ObjectInputStream _reader;

    @Override
    public void connect(String host, int port) throws Exception {
        _sock = new Socket(host, port);
        _sock.setSoTimeout(SOCKET_TIMEOUT);
        _writer = new ObjectOutputStream(_sock.getOutputStream());
        _reader = new ObjectInputStream(_sock.getInputStream());
    }

    @Override
    public void writeObject(Object object) throws Exception {
        _writer.writeObject(object);
    }

    @Override
    public Object readObject() throws Exception {
        return _reader.readObject();
    }

    @Override
    public void close() throws Exception {
        _sock.close();
    }
    
}
