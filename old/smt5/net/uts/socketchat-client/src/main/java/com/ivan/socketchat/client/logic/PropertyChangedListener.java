package com.ivan.socketchat.client.logic;

public interface PropertyChangedListener {
    void onPropertyChanged(ViewModelBase sender, String name);
}
