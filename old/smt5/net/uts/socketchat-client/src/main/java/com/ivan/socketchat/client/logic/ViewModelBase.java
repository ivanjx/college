package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

public abstract class ViewModelBase {
    private ArrayList<PropertyChangedListener> _propertyChangedListeners;

    public ViewModelBase() {
        _propertyChangedListeners = new ArrayList<PropertyChangedListener>();
    }

    protected void notifyChange(String propertyName) {
        for (PropertyChangedListener listener : _propertyChangedListeners) {
            listener.onPropertyChanged(this, propertyName);
        }
    }

    public void registerPropertyChangedListener(PropertyChangedListener listener) {
        _propertyChangedListeners.add(listener);
    }

    public void unregisterPropertyChangedListener(PropertyChangedListener listener) {
        _propertyChangedListeners.remove(listener);
    }
}
