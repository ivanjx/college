package com.ivan.socketchat.client.logic;

public class LoginViewModel extends ViewModelBase {
    public static final String HostProperty = "Host";
    public static final String PortProperty = "Port";
    public static final String UsernameProperty = "Username";

    private ChatService _chatService;
    private ModalService _modalService;

    private String _host;
    private Integer _port;
    private String _username;

    public LoginViewModel(
        ChatService chatService,
        ModalService modalService
    ) {
        _host = "";
        _port = 0;
        _username = "";

        _chatService = chatService;
        _modalService = modalService;
    }

    public String getHost() {
        return _host;
    }

    public void setHost(String host) {
        _host = host;
        notifyChange(HostProperty);
    }

    private void validateHost() throws Exception {
        if (getHost().equals("")) {
            throw new Exception("Host is empty");
        }

        if (getHost().contains(" ")) {
            throw new Exception("Invalid host name");
        }
    }

    public Integer getPort() {
        return _port;
    }

    public void setPort(Integer port) {
        _port = port;
        notifyChange(PortProperty);
    }

    private void validatePort() throws Exception {
        if (getPort() <= 0 || getPort() > 65535) {
            throw new Exception("Invalid port number");
        }
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
        notifyChange(UsernameProperty);
    }
    
    private void validateUsername() throws Exception {
        if (getUsername().equals("")) {
            throw new Exception("Username is empty");
        }
    }

    public void login() {
        try {
            // Validating.
            validateHost();
            validatePort();
            validateUsername();

            // Connecting.
            _chatService.connect(
                getHost(),
                getPort(),
                getUsername()
            );
        } catch (Exception e) {
            _modalService.showError("Error", e.getMessage());
            e.printStackTrace();
        }
    }
}
