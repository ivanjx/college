package com.ivan.socketchat.client.logic;

import javax.swing.DefaultListModel;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class ChatViewModel extends ViewModelBase implements LoginStatusChangedListener, UserConnectedListener, UserDisconnectedListener, NewMessageReceivedListener {
    public static final String MessageProperty = "Message";

    private ChatService _chatService;
    private ModalService _modalService;

    private DefaultListModel<User> _connectedUsers;
    private DefaultListModel<Message> _messages;
    private String _message;

    public ChatViewModel(
        ChatService chatService,
        ModalService modalService
    ) {
        _connectedUsers = new DefaultListModel<User>();
        _messages = new DefaultListModel<Message>();
        _message = "";

        _chatService = chatService;
        _modalService = modalService;

        _chatService.registerLoginStatusChangedListener(this);
        _chatService.registerUserConnectedListener(this);
        _chatService.registerUserDisconnectedListener(this);
        _chatService.registerNewMessageReceivedListener(this);
    }

    public DefaultListModel<User> getConnectedUsersModel() {
        return _connectedUsers;
    }

    public DefaultListModel<Message> getMessagesModel() {
        return _messages;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(String message) {
        _message = message;
        notifyChange(MessageProperty);
    }

    boolean isConnected() {
        try {
            _chatService.getUser();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void loadUsers() {
        if (!isConnected()) {
            return;
        }

        try {
            User[] users = _chatService.getUsers();
            
            synchronized(_connectedUsers) {
                for (User user : users) {
                    _connectedUsers.addElement(user);
                }
            }
        } catch (Exception e) {
            _modalService.showError("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    public void loadMessages() {
        if (!isConnected()) {
            return;
        }

        try {
            Message[] messages = _chatService.getMessages();

            synchronized(_messages) {
                for (Message message : messages) {
                    _messages.addElement(message);
                }
            }
        } catch (Exception e) {
            _modalService.showError("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    public void sendMessage() {
        if (!isConnected()) {
            return;
        }

        try {
            if (getMessage().equals("")) {
                return;
            }

            Message message = new Message();
            message.setMessage(getMessage());
            message = _chatService.addMessage(message);

            synchronized(_messages) {
                _messages.addElement(message);
            }

            setMessage("");
        } catch (Exception e) {
            _modalService.showError("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (!isConnected()) {
            return;
        }

        try {
            _chatService.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoginStatusChanged(boolean isLoggedIn) {
        synchronized(_messages) {
            _messages.clear();
        }

        synchronized(_connectedUsers) {
            _connectedUsers.clear();
        }

        setMessage("");

        if (isLoggedIn) {
            loadUsers();
            loadMessages();
        } else {
            _modalService.showError("Disconnected", "Disconnected from the chat server.");
        }
    }

    @Override
    public void onNewMessageReceived(Message message) {
        if (!isConnected()) {
            return;
        }

        synchronized(_messages) {
            _messages.addElement(message);
        }
    }

    @Override
    public void onUserDisconnected(User user) {
        if (!isConnected()) {
            return;
        }

        synchronized(_connectedUsers) {
            _connectedUsers.removeElement(user);
        }
    }

    @Override
    public void onUserConnected(User user) {
        if (!isConnected()) {
            return;
        }

        synchronized(_connectedUsers) {
            _connectedUsers.addElement(user);
        }
    }
}
