package com.ivan.socketchat.client.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.EOFException;
import java.time.LocalDateTime;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

import org.junit.Test;

public class ChatServiceImplTest {
    FakeConectionServiceImpl _conn;
    ChatServiceImpl _service;

    public ChatServiceImplTest() {
        _conn = new FakeConectionServiceImpl();
        _service = new ChatServiceImpl(_conn);
    }

    @Test
    public void getUser_rejectWithoutConnect() throws Exception {
        // Test.
        Exception getUserException = null;

        try {
            _service.getUser();
        } catch (Exception ex) {
            getUserException = ex;
        }

        // Assert.
        assertNotNull(getUserException);
        assertEquals("Not connected", getUserException.getMessage());
    }

    @Test
    public void connect_test() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Test.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );

        // Assert.
        assertEquals("localhost", _conn.connectHostArg);
        assertEquals(1234, _conn.connectPortArg);
        assertEquals(0, _conn.closeCallCount);

        assertNotNull(_service.getUser());
        assertEquals("ivan", _service.getUser().getUsername());
        
        assertEquals(2, _conn.writeObjectArgs.size());
        assertEquals("SET_IDENTITY", _conn.writeObjectArgs.get(0));
        assertTrue(_conn.writeObjectArgs.get(1) instanceof User);
        User userArg = (User)_conn.writeObjectArgs.get(1);
        assertEquals("ivan", userArg.getUsername());

        assertEquals(1, listener.loginStatusChanges.size());
        assertTrue(listener.loginStatusChanges.get(0));
    }

    @Test
    public void connect_handleConnectionError() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Test.
        _conn.connectException = new Exception("WOOF");
        Exception connectException = null;
        
        try {
            _service.connect(
                "localhost",
                1234,
                "ivan"
            );
        } catch (Exception ex) {
            connectException = ex;
        }
        
        // Assert.
        assertNotNull(connectException);
        assertEquals("WOOF", connectException.getMessage());
        assertEquals(0, _conn.writeObjectArgs.size());
        assertEquals(0, listener.loginStatusChanges.size());

        // Can try again.
        _conn.connectException = null;
        connectException = null;

        try {
            _service.connect(
                "localhost",
                1234,
                "ivan"
            );
        } catch (Exception ex) {
            connectException = ex;
        }

        // Assert.
        assertEquals("localhost", _conn.connectHostArg);
        assertEquals(1234, _conn.connectPortArg);
        assertEquals(0, _conn.closeCallCount);
        
        assertEquals(2, _conn.writeObjectArgs.size());
        assertEquals("SET_IDENTITY", _conn.writeObjectArgs.get(0));
        assertTrue(_conn.writeObjectArgs.get(1) instanceof User);
        User userArg = (User)_conn.writeObjectArgs.get(1);
        assertEquals("ivan", userArg.getUsername());
        
        assertEquals(1, listener.loginStatusChanges.size());
        assertTrue(listener.loginStatusChanges.get(0));
    }

    @Test
    public void connect_rejectMultipleConnectCalls() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        listener.loginStatusChanges.clear();

        // Test.
        Exception connectException = null;

        try {
            _service.connect(
                "localhost",
                1234,
                "john"
            );
        } catch (Exception ex) {
            connectException = ex;
        }

        // Assert.
        assertNotNull(connectException);
        assertEquals("Already connected", connectException.getMessage());

        assertEquals(2, _conn.writeObjectArgs.size());
        assertEquals("SET_IDENTITY", _conn.writeObjectArgs.get(0));
        assertTrue(_conn.writeObjectArgs.get(1) instanceof User);
        User userArg = (User)_conn.writeObjectArgs.get(1);
        assertEquals("ivan", userArg.getUsername());

        assertEquals(0, listener.loginStatusChanges.size());
    }

    @Test
    public void disconnect_test() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        listener.loginStatusChanges.clear();

        // Test.
        _service.disconnect();

        // Assert.
        assertEquals(1, _conn.closeCallCount);
        assertEquals(3, _conn.writeObjectArgs.size());
        assertEquals("DISCONNECT", _conn.writeObjectArgs.get(2));
        
        assertEquals(1, listener.loginStatusChanges.size());
        assertFalse(listener.loginStatusChanges.get(0));
    }

    @Test
    public void disconnect_rejectCallWithoutConnect() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Test.
        Exception disconnectException = null;

        try {
            _service.disconnect();
        } catch (Exception ex) {
            disconnectException = ex;
        }

        // Assert.
        assertNotNull(disconnectException);
        assertEquals("Not connected", disconnectException.getMessage());
        assertEquals(0, _conn.closeCallCount);
        assertEquals(0, _conn.writeObjectArgs.size());
        assertEquals(0, listener.loginStatusChanges.size());
    }

    @Test
    public void disconnect_forceDisconnectOnError() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();
        listener.loginStatusChanges.clear();

        // Test error on writeObject.
        _conn.writeObjectException = new Exception("WOOF");
        _service.disconnect();

        // Assert.
        assertEquals(0, _conn.writeObjectArgs.size());
        assertEquals(1, _conn.closeCallCount);
        assertEquals(1, listener.loginStatusChanges.size());
        Exception getUserException = null;

        try {
            _service.getUser();
        } catch (Exception ex) {
            getUserException = ex;
        }

        assertNotNull(getUserException);
        assertEquals("Not connected", getUserException.getMessage());

        // Initial connect.
        _conn.writeObjectException = null;
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();
        listener.loginStatusChanges.clear();

        // Test error on close.
        _conn.closeException = new Exception("WOOF");
        _service.disconnect();

        // Assert.
        assertEquals(1, _conn.writeObjectArgs.size());
        assertEquals("DISCONNECT", _conn.writeObjectArgs.get(0));
        assertEquals(1, _conn.closeCallCount);
        assertEquals(1, listener.loginStatusChanges.size());
        getUserException = null;

        try {
            _service.getUser();
        } catch (Exception ex) {
            getUserException = ex;
        }

        assertNotNull(getUserException);
        assertEquals("Not connected", getUserException.getMessage());
    }

    @Test
    public void disconnect_canReconnectAfterDisconnect() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Test.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _service.disconnect();
        _service.connect(
            "localhost",
            1234,
            "john"
        );

        // Assert.
        assertEquals("localhost", _conn.connectHostArg);
        assertEquals(1234, _conn.connectPortArg);
        assertEquals(1, _conn.closeCallCount);
        
        assertEquals(5, _conn.writeObjectArgs.size());

        assertEquals("SET_IDENTITY", _conn.writeObjectArgs.get(0));
        assertTrue(_conn.writeObjectArgs.get(1) instanceof User);
        User userArg = (User)_conn.writeObjectArgs.get(1);
        assertEquals("ivan", userArg.getUsername());

        assertEquals("DISCONNECT", _conn.writeObjectArgs.get(2));

        assertEquals("SET_IDENTITY", _conn.writeObjectArgs.get(3));
        assertTrue(_conn.writeObjectArgs.get(4) instanceof User);
        userArg = (User)_conn.writeObjectArgs.get(4);
        assertEquals("john", userArg.getUsername());

        assertEquals(3, listener.loginStatusChanges.size());
        assertTrue(listener.loginStatusChanges.get(0));
        assertFalse(listener.loginStatusChanges.get(1));
        assertTrue(listener.loginStatusChanges.get(2));
    }

    @Test
    public void ping_test() throws Exception {
        // Ping response.
        _conn.readObjectResults.add("OK");

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();

        // Test.
        _service.ping();

        // Assert.
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(1, _conn.writeObjectArgs.size());
        assertEquals("PING", _conn.writeObjectArgs.get(0));
    }

    @Test
    public void ping_rejectWithoutConnect() throws Exception {
        // Ping response.
        _conn.readObjectResults.add("OK");

        // Test.
        Exception pingException = null;

        try {
            _service.ping();
        } catch (Exception ex) {
            pingException = ex;
        }

        // Assert.
        assertNotNull(pingException);
        assertEquals("Not connected", pingException.getMessage());
        assertEquals(1, _conn.readObjectResults.size());
        assertEquals(0, _conn.writeObjectArgs.size());
    }

    @Test
    public void getUsers_test() throws Exception {
        // Response.
        User user = new User();
        user.setUsername("ivan");
        User user2 = new User();
        user2.setUsername("john");
        _conn.readObjectResults.add(
            new User[] { user, user2 }
        );

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();

        // Test.
        User[] result = _service.getUsers();

        // Assert.
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(1, _conn.writeObjectArgs.size());
        assertEquals("GET_USERS", _conn.writeObjectArgs.get(0));
        assertEquals(2, result.length);
        assertEquals(user, result[0]);
        assertEquals(user2, result[1]);
    }

    @Test
    public void getUsers_rejectWithoutConnect() throws Exception {
        // Response.
        User user = new User();
        user.setUsername("ivan");
        User user2 = new User();
        user2.setUsername("john");
        _conn.readObjectResults.add(
            new User[] { user, user2 }
        );

        // Test.
        Exception getUsersException = null;

        try {
            _service.getUsers();
        } catch (Exception ex) {
            getUsersException = ex;
        }

        // Assert.
        assertNotNull(getUsersException);
        assertEquals("Not connected", getUsersException.getMessage());
        assertEquals(1, _conn.readObjectResults.size());
        assertEquals(0, _conn.writeObjectArgs.size());
    }

    @Test
    public void getMessages_test() throws Exception {
        // Response.
        User user = new User();
        user.setUsername("ivan");
        Message msg = new Message();
        msg.setId(1);
        msg.setSender(user);
        msg.setTime(LocalDateTime.now());
        msg.setMessage("hello");
        Message msg2 = new Message();
        msg2.setId(2);
        msg2.setSender(user);
        msg2.setTime(LocalDateTime.now());
        msg2.setMessage("hello 2");
        _conn.readObjectResults.add(
            new Message[] { msg, msg2 }
        );

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();

        // Test.
        Message[] result = _service.getMessages();

        // Assert.
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(1, _conn.writeObjectArgs.size());
        assertEquals("GET_MESSAGES", _conn.writeObjectArgs.get(0));
        assertEquals(2, result.length);
        assertEquals(msg, result[0]);
        assertEquals(msg2, result[1]);
    }

    @Test
    public void getMessages_rejectWithoutConnect() throws Exception {
        // Response.
        User user = new User();
        user.setUsername("ivan");
        Message msg = new Message();
        msg.setId(1);
        msg.setSender(user);
        msg.setTime(LocalDateTime.now());
        msg.setMessage("hello");
        _conn.readObjectResults.add(
            new Message[] { msg }
        );

        // Test.
        Exception getMessagesException = null;

        try {
            _service.getMessages();
        } catch (Exception ex) {
            getMessagesException = ex;
        }

        // Assert.
        assertNotNull(getMessagesException);
        assertEquals("Not connected", getMessagesException.getMessage());
        assertEquals(1, _conn.readObjectResults.size());
        assertEquals(0, _conn.writeObjectArgs.size());
    }

    @Test
    public void addMessage_test() throws Exception {
        // Response.
        User responseSender = new User();
        responseSender.setUsername("sender");
        Message response = new Message();
        response.setId(1);
        response.setSender(responseSender);
        response.setTime(LocalDateTime.now());
        response.setMessage("hello");
        _conn.readObjectResults.add(response);

        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();

        // Test.
        Message message = new Message();
        message.setMessage("hello");
        Message result = _service.addMessage(message);

        // Assert.
        assertEquals(response, result);
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(2, _conn.writeObjectArgs.size());
        assertEquals("ADD_MESSAGE", _conn.writeObjectArgs.get(0));
        assertTrue(_conn.writeObjectArgs.get(1) instanceof Message);
        Message arg = (Message)_conn.writeObjectArgs.get(1);
        assertEquals(0, arg.getId()); // Will be set by the server.
        assertNull(arg.getTime()); // Will be set by the server.
        assertEquals(_service.getUser(), arg.getSender());
        assertEquals("hello", arg.getMessage());
    }

    @Test
    public void addMessage_rejectWithoutConnect() throws Exception {
        // Response.
        Message response = new Message();
        response.setId(1);
        response.setSender(new User());
        response.setTime(LocalDateTime.now());
        response.setMessage("hello");
        _conn.readObjectResults.add(response);

        // Test.
        Exception addMessageException = null;

        try {
            Message message = new Message();
            message.setMessage("hello");
            _service.addMessage(message);
        } catch (Exception ex) {
            addMessageException = ex;
        }

        // Assert.
        assertNotNull(addMessageException);
        assertEquals("Not connected", addMessageException.getMessage());
        assertEquals(1, _conn.readObjectResults.size());
        assertEquals(0, _conn.writeObjectArgs.size());
    }

    @Test
    public void addMessage_rejectEmptyMessage() throws Exception {
        // Initial connect.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        _conn.writeObjectArgs.clear();

        // Test null.
        Exception addMessageException = null;

        try {
            Message message = new Message();
            _service.addMessage(message);
        } catch (Exception ex) {
            addMessageException = ex;
        }

        assertNotNull(addMessageException);
        assertEquals("Invalid message", addMessageException.getMessage());
        assertEquals(0, _conn.writeObjectArgs.size());

        // Test empty.
        addMessageException = null;

        try {
            Message message = new Message();
            message.setMessage("");
            _service.addMessage(message);
        } catch (Exception ex) {
            addMessageException = ex;
        }

        assertNotNull(addMessageException);
        assertEquals("Invalid message", addMessageException.getMessage());
        assertEquals(0, _conn.writeObjectArgs.size());
    }

    @Test
    public void read_multiplexing() throws Exception {
        // Responses.
        _conn.readObjectResults.add("OK"); // PING.

        _conn.readObjectResults.add("USER_CONNECT");
        User userConnect1 = new User();
        userConnect1.setUsername("john");
        _conn.readObjectResults.add(userConnect1);
        
        _conn.readObjectResults.add("USER_CONNECT");
        User userConnect2 = new User();
        userConnect2.setUsername("doe");
        _conn.readObjectResults.add(userConnect2);

        _conn.readObjectResults.add(new User[] { userConnect1, userConnect2 }); // GET_USERS.

        _conn.readObjectResults.add("USER_DISCONNECT");
        _conn.readObjectResults.add(userConnect2);

        _conn.readObjectResults.add("USER_DISCONNECT");
        _conn.readObjectResults.add("WOOF"); // Test invalid event.

        Message receivedMessage = new Message();
        receivedMessage.setId(10);
        _conn.readObjectResults.add("NEW_MESSAGE");
        _conn.readObjectResults.add(receivedMessage);

        Message message = new Message();
        message.setId(1);
        _conn.readObjectResults.add(new Message[] { message }); // GET_MESSAGES.

        _conn.readObjectResults.add("OK"); // PING.

        // Listeners.
        UserConnectedListenerImpl userConnectedListener = new UserConnectedListenerImpl();
        UserDisconnectedListenerImpl userDisconnectedListener = new UserDisconnectedListenerImpl();
        NewMessageReceivedListenerImpl newMessageReceivedListener = new NewMessageReceivedListenerImpl();
        _service.registerUserConnectedListener(userConnectedListener);
        _service.registerUserDisconnectedListener(userDisconnectedListener);
        _service.registerNewMessageReceivedListener(newMessageReceivedListener);

        // Connecting.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );

        // Test ping.
        _service.ping();
        final Exception[] ping2Exception = new Exception[1]; // Java sucks...
        Thread ping2Thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    _service.ping();
                } catch (Exception e) {
                    ping2Exception[0] = e;
                }
            }
        });
        ping2Thread.start();

        // Test get users.
        User[] getUsersResult = _service.getUsers();
        assertNotNull(getUsersResult);
        assertEquals(2, getUsersResult.length);
        assertEquals(userConnect1, getUsersResult[0]);
        assertEquals(userConnect2, getUsersResult[1]);

        // Test get messages.
        Message[] getMessagesResult = _service.getMessages();
        assertNotNull(getMessagesResult);
        assertEquals(1, getMessagesResult.length);
        assertEquals(message.getId(), getMessagesResult[0].getId());

        // Wait for second ping if still running.
        ping2Thread.join();
        assertNull(ping2Exception[0]);

        // Done.
        assertEquals(0, _conn.readObjectResults.size());

        // Assert events.
        assertEquals(2, userConnectedListener.connectedUsers.size());
        assertEquals(userConnect1, userConnectedListener.connectedUsers.get(0));
        assertEquals(userConnect2, userConnectedListener.connectedUsers.get(1));

        assertEquals(1, userDisconnectedListener.disconnectedUsers.size());
        assertEquals(userConnect2, userDisconnectedListener.disconnectedUsers.get(0));

        assertEquals(1, newMessageReceivedListener.receivedMessages.size());
        assertEquals(10, newMessageReceivedListener.receivedMessages.get(0).getId());
    }

    @Test
    public void read_disconnectOnEOFError() throws Exception {
        // Listener.
        LoginStatusChangedListenerImpl listener = new LoginStatusChangedListenerImpl();
        _service.registerLoginStatusChangedListener(listener);

        // Responses.
        _conn.readObjectResults.add("OK"); // PING.
        _conn.readObjectResults.add(new EOFException("EOF"));

        // Should disconnect even on error.
        _conn.closeException = new Exception("WOOF");

        // Test.
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(2, listener.loginStatusChanges.size());
        assertTrue(listener.loginStatusChanges.get(0));
        assertFalse(listener.loginStatusChanges.get(1));

        Exception getUserException = null;

        try {
            _service.getUser();
        } catch (Exception ex) {
            getUserException = ex;
        }

        assertNotNull(getUserException);
        assertEquals("Not connected", getUserException.getMessage());
    }

    @Test
    public void unregisterEvents() throws Exception {
        // Responses.
        _conn.readObjectResults.add("USER_CONNECT");
        User userConnect = new User();
        userConnect.setUsername("john");
        _conn.readObjectResults.add(userConnect);

        _conn.readObjectResults.add("USER_DISCONNECT");
        _conn.readObjectResults.add(userConnect);

        Message receivedMessage = new Message();
        receivedMessage.setId(10);
        _conn.readObjectResults.add("NEW_MESSAGE");
        _conn.readObjectResults.add(receivedMessage);

        // Listeners.
        UserConnectedListenerImpl userConnectedListener = new UserConnectedListenerImpl();
        UserDisconnectedListenerImpl userDisconnectedListener = new UserDisconnectedListenerImpl();
        NewMessageReceivedListenerImpl newMessageReceivedListener = new NewMessageReceivedListenerImpl();
        _service.registerUserConnectedListener(userConnectedListener);
        _service.registerUserDisconnectedListener(userDisconnectedListener);
        _service.registerNewMessageReceivedListener(newMessageReceivedListener);

        // Test.
        _service.unregisterUserConnectedListener(userConnectedListener);
        _service.unregisterUserDisconnectedListener(userDisconnectedListener);
        _service.unregisterNewMessageReceivedListener(newMessageReceivedListener);
        _service.connect(
            "localhost",
            1234,
            "ivan"
        );
        Thread.sleep(100);

        // Assert.
        assertEquals(0, _conn.readObjectResults.size());
        assertEquals(0, userConnectedListener.connectedUsers.size());
        assertEquals(0, userDisconnectedListener.disconnectedUsers.size());
        assertEquals(0, newMessageReceivedListener.receivedMessages.size());
    }
}
