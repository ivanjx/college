package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

public class FakeConectionServiceImpl implements ConnectionService {

    public String connectHostArg;
    public int connectPortArg;
    public Exception connectException;
    public ArrayList<Object> writeObjectArgs;
    public Exception writeObjectException;
    public ArrayList<Object> readObjectResults;
    public Exception readObjectException;
    public Exception closeException;
    public int closeCallCount;

    public FakeConectionServiceImpl() {
        writeObjectArgs = new ArrayList<>();
        readObjectResults = new ArrayList<>();
    }

    @Override
    public void connect(String host, int port) throws Exception {
        if (connectException != null) {
            throw connectException;
        }

        connectHostArg = host;
        connectPortArg = port;
    }

    @Override
    public void writeObject(Object object) throws Exception {
        if (writeObjectException != null) {
            throw writeObjectException;
        }

        writeObjectArgs.add(object);
    }

    @Override
    public Object readObject() throws Exception {
        if (readObjectException != null) {
            throw readObjectException;
        }

        if (readObjectResults.size() == 0) {
            Thread.sleep(1000 * 1000);
        }

        Object ret = readObjectResults.get(0);
        readObjectResults.remove(0);

        if (ret instanceof Exception) {
            throw (Exception)ret;
        }

        return ret;
    }

    @Override
    public void close() throws Exception {
        if (closeException != null) {
            throw closeException;
        }
        
        ++closeCallCount;
    }
    
}
