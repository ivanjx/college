package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

public class FakeChatServiceImpl implements ChatService {

    public Exception connectException;
    public String connectHost;
    public int connectPort;
    public String connectUsername;
    public Exception disconnectException;
    public int disconnectCount;
    public Exception getUserException;
    public User getUserResult;
    public Exception pingException;
    public int pingCount;
    public Exception getUsersException;
    public User[] getUsersResult;
    public Exception getMessagesException;
    public Message[] getMessagesResult;
    public Exception addMessageException;
    public ArrayList<Message> addMessageArgs;
    public ArrayList<Message> addMessageResults;
    public ArrayList<LoginStatusChangedListener> registeredLoginStatusChangedListeners;
    public ArrayList<LoginStatusChangedListener> unregisteredLoginStatusChangedListeners;
    public ArrayList<UserConnectedListener> registeredUserConnectedListeners;
    public ArrayList<UserConnectedListener> unregisteredUserConnectedListeners;
    public ArrayList<UserDisconnectedListener> registeredUserDisconnectedListeners;
    public ArrayList<UserDisconnectedListener> unregisteredUserDisconnectedListeners;
    public ArrayList<NewMessageReceivedListener> registeredNewMessageReceivedListeners;
    public ArrayList<NewMessageReceivedListener> unregisteredNewMessageReceivedListeners;

    public FakeChatServiceImpl() {
        addMessageArgs = new ArrayList<>();
        addMessageResults = new ArrayList<>();
        registeredLoginStatusChangedListeners = new ArrayList<>();
        unregisteredLoginStatusChangedListeners = new ArrayList<>();
        registeredUserConnectedListeners = new ArrayList<>();
        unregisteredUserConnectedListeners = new ArrayList<>();
        registeredUserDisconnectedListeners = new ArrayList<>();
        unregisteredUserDisconnectedListeners = new ArrayList<>();
        registeredNewMessageReceivedListeners = new ArrayList<>();
        unregisteredNewMessageReceivedListeners = new ArrayList<>();
    }

    @Override
    public void connect(String host, int port, String username) throws Exception {
        if (connectException != null) {
            throw connectException;
        }

        connectHost = host;
        connectPort = port;
        connectUsername = username;
    }

    @Override
    public void disconnect() throws Exception {
        if (disconnectException != null) {
            throw disconnectException;
        }

        ++disconnectCount;
    }

    @Override
    public User getUser() throws Exception {
        if (getUserException != null) {
            throw getUserException;
        }

        return getUserResult;
    }

    @Override
    public void ping() throws Exception {
        if (pingException != null) {
            throw pingException;
        }

        ++pingCount;
    }

    @Override
    public User[] getUsers() throws Exception {
        if (getUsersException != null) {
            throw getUsersException;
        }

        return getUsersResult;
    }

    @Override
    public Message[] getMessages() throws Exception {
        if (getMessagesException != null) {
            throw getMessagesException;
        }

        return getMessagesResult;
    }

    @Override
    public Message addMessage(Message message) throws Exception {
        if (addMessageException != null) {
            throw addMessageException;
        }

        addMessageArgs.add(message);
        Message ret = addMessageResults.get(0);
        addMessageResults.remove(0);
        return ret;
    }

    @Override
    public void registerLoginStatusChangedListener(LoginStatusChangedListener listener) {
        registeredLoginStatusChangedListeners.add(listener);
    }

    @Override
    public void unregisterLoginStatusChangedListener(LoginStatusChangedListener listener) {
        unregisteredLoginStatusChangedListeners.add(listener);
    }

    @Override
    public void registerUserConnectedListener(UserConnectedListener listener) {
        registeredUserConnectedListeners.add(listener);
    }

    @Override
    public void registerUserDisconnectedListener(UserDisconnectedListener listener) {
        registeredUserDisconnectedListeners.add(listener);
    }

    @Override
    public void registerNewMessageReceivedListener(NewMessageReceivedListener listener) {
        registeredNewMessageReceivedListeners.add(listener);
    }

    @Override
    public void unregisterUserConnectedListener(UserConnectedListener listener) {
        unregisteredUserConnectedListeners.add(listener);
    }

    @Override
    public void unregisterUserDisconnectedListener(UserDisconnectedListener listener) {
        unregisteredUserDisconnectedListeners.add(listener);
    }

    @Override
    public void unregisterNewMessageReceivedListener(NewMessageReceivedListener listener) {
        unregisteredNewMessageReceivedListeners.add(listener);
    }

    public void broadcastLoginStatusChanged(boolean isLoggedIn) {
        for (LoginStatusChangedListener listener : registeredLoginStatusChangedListeners) {
            listener.onLoginStatusChanged(isLoggedIn);
        }
    }

    public void broadcastUserConnected(User user) {
        for (UserConnectedListener listener : registeredUserConnectedListeners) {
            listener.onUserConnected(user);
        }
    }

    public void broadcastUserDisconnected(User user) {
        for (UserDisconnectedListener listener : registeredUserDisconnectedListeners) {
            listener.onUserDisconnected(user);
        }
    }

    public void broadcastNewMessageReceived(Message message) {
        for (NewMessageReceivedListener listener : registeredNewMessageReceivedListeners) {
            listener.onNewMessageReceived(message);
        }
    }
    
}
