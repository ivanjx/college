package com.ivan.socketchat.client.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;

import com.ivan.socketchat.contract.Message;
import com.ivan.socketchat.contract.User;

import org.junit.Test;

public class ChatViewModelTest {
    FakeChatServiceImpl _chatService;
    FakeModalServiceImpl _modalService;
    ChatViewModel _vm;

    public ChatViewModelTest() {
        _chatService = new FakeChatServiceImpl();
        _modalService = new FakeModalServiceImpl();
        _vm = new ChatViewModel(
            _chatService,
            _modalService
        );
    }

    @Test
    public void registerChatServiceEvents() throws Exception {
        assertEquals(1, _chatService.registeredLoginStatusChangedListeners.size());
        assertEquals(_vm, _chatService.registeredLoginStatusChangedListeners.get(0));

        assertEquals(1, _chatService.registeredUserConnectedListeners.size());
        assertEquals(_vm, _chatService.registeredUserConnectedListeners.get(0));

        assertEquals(1, _chatService.registeredUserDisconnectedListeners.size());
        assertEquals(_vm, _chatService.registeredUserDisconnectedListeners.get(0));

        assertEquals(1, _chatService.registeredNewMessageReceivedListeners.size());
        assertEquals(_vm, _chatService.registeredNewMessageReceivedListeners.get(0));

        assertEquals(0, _chatService.unregisteredLoginStatusChangedListeners.size());
        assertEquals(0, _chatService.unregisteredUserConnectedListeners.size());
        assertEquals(0, _chatService.unregisteredUserDisconnectedListeners.size());
        assertEquals(0, _chatService.unregisteredNewMessageReceivedListeners.size());
    }

    @Test
    public void connectedUsersModel_test() throws Exception {
        assertNotNull(_vm.getConnectedUsersModel());
        assertEquals(0, _vm.getConnectedUsersModel().size());
    }

    @Test
    public void messagesModel_test() throws Exception {
        assertNotNull(_vm.getMessagesModel());
        assertEquals(0, _vm.getMessagesModel().size());
    }

    @Test
    public void message_property_test() throws Exception {
        // Listener.
        PropertyChangedListenerImpl listener = new PropertyChangedListenerImpl();
        _vm.registerPropertyChangedListener(listener);

        // Test.
        assertEquals("", _vm.getMessage());

        _vm.setMessage("hello");
        assertEquals("hello", _vm.getMessage());
        assertEquals(1, listener.changedPropertyNames.size());
        assertEquals(ChatViewModel.MessageProperty, listener.changedPropertyNames.get(0));
    }

    @Test
    public void loadUsers_test() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        User user2 = new User();
        user2.setUsername("john");
        _chatService.getUsersResult = new User[] {
            user, user2
        };
        _chatService.getUserResult = user;

        // Test.
        _vm.loadUsers();

        // Assert.
        assertEquals(2, _vm.getConnectedUsersModel().size());
        assertEquals(user, _vm.getConnectedUsersModel().get(0));
        assertEquals(user2, _vm.getConnectedUsersModel().get(1));
        assertNull(_modalService.showErrorMessage);
    }

    @Test 
    public void loadUsers_handleNotConnected() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        User user2 = new User();
        user2.setUsername("john");
        _chatService.getUsersResult = new User[] {
            user, user2
        };
        _chatService.getUserException = new Exception();

        // Test.
        _vm.loadUsers();

        // Assert.
        assertEquals(0, _vm.getConnectedUsersModel().size());
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void loadUsers_handleError() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUserResult = user;
        _chatService.getUsersException = new Exception("WOOF");

        // Test.
        _vm.loadUsers();

        // Assert.
        assertEquals(0, _vm.getConnectedUsersModel().size());
        assertEquals("WOOF", _modalService.showErrorMessage);
    }

    @Test
    public void loadMessages_test() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        Message message2 = new Message();
        message2.setId(2);
        message2.setSender(user);
        message2.setTime(LocalDateTime.now());
        message2.setMessage("hello2");
        _chatService.getMessagesResult = new Message[] {
            message, message2
        };
        _chatService.getUserResult = user;

        // Test.
        _vm.loadMessages();

        // Assert.
        assertEquals(2, _vm.getMessagesModel().size());
        assertEquals(message, _vm.getMessagesModel().get(0));
        assertEquals(message2, _vm.getMessagesModel().get(1));
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void loadMessages_handleNotConnected() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message message = new Message();
        message.setId(1);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        _chatService.getMessagesResult = new Message[] {
            message
        };
        _chatService.getUserException = new Exception();

        // Test.
        _vm.loadMessages();

        // Assert.
        assertEquals(0, _vm.getMessagesModel().size());
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void loadMessages_handleError() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUserResult = user;
        _chatService.getMessagesException = new Exception("WOOF");

        // Test.
        _vm.loadMessages();

        // Assert.
        assertEquals(0, _vm.getMessagesModel().size());
        assertEquals("WOOF", _modalService.showErrorMessage);
    }

    @Test
    public void disconnect_test() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUserResult = user;

        // Test.
        _vm.disconnect();

        // Assert.
        assertEquals(1, _chatService.disconnectCount);
    }

    @Test
    public void disconnect_handleError() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUserResult = user;
        _chatService.disconnectException = new Exception();

        // Test.
        _vm.disconnect();
        
        // Assert.
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void disconnect_handleNotConnected() throws Exception {
        // Sample response.
        _chatService.getUserException = new Exception();

        // Test.
        _vm.disconnect();

        // Assert.
        assertEquals(0, _chatService.disconnectCount);
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void sendMessage_test() {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message initial = new Message();
        initial.setId(1);
        initial.setSender(user);
        initial.setTime(LocalDateTime.now());
        initial.setMessage("initial");
        _chatService.getMessagesResult = new Message[] {
            initial
        };
        Message response = new Message();
        response.setId(10);
        response.setSender(user);
        response.setTime(LocalDateTime.now());
        response.setMessage("hello");
        _chatService.addMessageResults.add(response);
        Message response2 = new Message();
        response2.setId(11);
        response2.setSender(user);
        response2.setTime(LocalDateTime.now());
        response2.setMessage("hello2");
        _chatService.addMessageResults.add(response2);
        _chatService.getUserResult = user;

        // Initial message load.
        _vm.loadMessages();

        // Test.
        _vm.setMessage("hello");
        _vm.sendMessage();
        _vm.setMessage("hello2");
        _vm.sendMessage();

        // Assert.
        assertEquals(0, _chatService.addMessageResults.size());
        assertEquals(2, _chatService.addMessageArgs.size());
        assertNull(_chatService.addMessageArgs.get(0).getSender());
        assertNull(_chatService.addMessageArgs.get(0).getTime());
        assertEquals("hello", _chatService.addMessageArgs.get(0).getMessage());
        assertEquals("hello2", _chatService.addMessageArgs.get(1).getMessage());

        assertEquals(3, _vm.getMessagesModel().size());
        assertEquals(initial, _vm.getMessagesModel().get(0));
        assertEquals(response, _vm.getMessagesModel().get(1));
        assertEquals(response2, _vm.getMessagesModel().get(2));
        assertEquals("", _vm.getMessage());
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void sendMessage_ignoreEmptyMessage() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message response = new Message();
        response.setId(10);
        response.setSender(user);
        response.setTime(LocalDateTime.now());
        response.setMessage("hello");
        _chatService.addMessageResults.add(response);
        _chatService.getUserResult = user;

        // Test.
        _vm.sendMessage();

        // Assert.
        assertEquals(0, _chatService.addMessageArgs.size());
        assertEquals(0, _vm.getMessagesModel().size());
        assertEquals("", _vm.getMessage());
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void sendMessage_handleError() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message initial = new Message();
        initial.setId(1);
        initial.setSender(user);
        initial.setTime(LocalDateTime.now());
        initial.setMessage("initial");
        _chatService.getMessagesResult = new Message[] {
            initial
        };
        _chatService.addMessageException = new Exception("WOOF");
        _chatService.getUserResult = user;

        // Initial message load.
        _vm.loadMessages();

        // Test.
        _vm.setMessage("hello");
        _vm.sendMessage();

        // Assert.
        assertEquals("WOOF", _modalService.showErrorMessage);
        assertEquals(1, _vm.getMessagesModel().size());
        assertEquals(initial, _vm.getMessagesModel().get(0));
    }

    @Test
    public void sendMessage_handleNotConnected() throws Exception {
        // Sample response.
        _chatService.getUserException = new Exception();

        // Test.
        _vm.setMessage("hello");
        _vm.sendMessage();

        // Assert.
        assertEquals(0, _chatService.addMessageArgs.size());
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void onUserConnected_test() throws Exception {
        // Sample response.
        User initial = new User();
        initial.setUsername("ivan");
        User user = new User();
        user.setUsername("john");
        _chatService.getUsersResult = new User[] {
            initial
        };
        _chatService.getUserResult = initial;

        // Initial users load.
        _vm.loadUsers();

        // Test.
        _chatService.broadcastUserConnected(user);

        // Assert.
        assertEquals(2, _vm.getConnectedUsersModel().size());
        assertEquals(initial, _vm.getConnectedUsersModel().get(0));
        assertEquals(user, _vm.getConnectedUsersModel().get(1));
    }

    @Test
    public void onUserConnected_ignoreOnDisconnected() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        _chatService.getUserException = new Exception();

        // Test.
        _chatService.broadcastUserConnected(user);

        // Assert.
        assertEquals(0, _vm.getConnectedUsersModel().size());
    }

    @Test
    public void onUserDisconnected_test() throws Exception {
        // Sample response.
        User initial = new User();
        initial.setUsername("ivan");
        User user = new User();
        user.setUsername("john");
        _chatService.getUsersResult = new User[] {
            initial, user
        };
        _chatService.getUserResult = initial;

        // Initial users load.
        _vm.loadUsers();

        // Test.
        _chatService.broadcastUserDisconnected(user);

        // Assert.
        assertEquals(1, _vm.getConnectedUsersModel().size());
        assertEquals(initial, _vm.getConnectedUsersModel().get(0));
    }

    @Test
    public void onUserDisconnected_ignoreOnDisconnected() throws Exception {
        // Sample response.
        User initial = new User();
        initial.setUsername("ivan");
        User user = new User();
        user.setUsername("john");
        _chatService.getUsersResult = new User[] {
            initial, user
        };
        _chatService.getUserResult = initial;

        // Initial users load.
        _vm.loadUsers();

        // Test.
        _chatService.getUserException = new Exception();
        _chatService.broadcastUserDisconnected(user);

        // Assert.
        assertEquals(2, _vm.getConnectedUsersModel().size());
        assertEquals(initial, _vm.getConnectedUsersModel().get(0));
        assertEquals(user, _vm.getConnectedUsersModel().get(1));
    }

    @Test
    public void onNewmessageReceived_test() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message initial = new Message();
        initial.setId(1);
        initial.setSender(user);
        initial.setTime(LocalDateTime.now());
        initial.setMessage("initial");
        _chatService.getMessagesResult = new Message[] {
            initial
        };
        Message message = new Message();
        message.setId(10);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        _chatService.getUserResult = user;

        // Initial message load.
        _vm.loadMessages();

        // Test.
        _chatService.broadcastNewMessageReceived(message);

        // Assert.
        assertEquals(2, _vm.getMessagesModel().size());
        assertEquals(initial, _vm.getMessagesModel().get(0));
        assertEquals(message, _vm.getMessagesModel().get(1));
    }

    @Test
    public void onNewMessageReceived_ignoreOnDisconnected() throws Exception {
        // Sample response.
        User user = new User();
        user.setUsername("ivan");
        Message message = new Message();
        message.setId(10);
        message.setSender(user);
        message.setTime(LocalDateTime.now());
        message.setMessage("hello");
        _chatService.getUserException = new Exception();

        // Test.
        _chatService.broadcastNewMessageReceived(message);

        // Assert.
        assertEquals(0, _vm.getMessagesModel().size());
    }
}
