package com.ivan.socketchat.client.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class LoginViewModelTest {
    FakeChatServiceImpl _chatService;
    FakeModalServiceImpl _modalService;
    LoginViewModel _vm;

    public LoginViewModelTest() {
        _chatService = new FakeChatServiceImpl();
        _modalService = new FakeModalServiceImpl();
        _vm = new LoginViewModel(
            _chatService,
            _modalService
        );
    }

    @Test
    public void host_property_test() {
        // Listener.
        PropertyChangedListenerImpl listener = new PropertyChangedListenerImpl();
        _vm.registerPropertyChangedListener(listener);

        // Test.
        assertEquals("", _vm.getHost());

        _vm.setHost("localhost");
        assertEquals("localhost", _vm.getHost());
        assertEquals(1, listener.changedPropertyNames.size());
        assertEquals(LoginViewModel.HostProperty, listener.changedPropertyNames.get(0));
    }

    @Test
    public void port_property_test() {
        // Listener.
        PropertyChangedListenerImpl listener = new PropertyChangedListenerImpl();
        _vm.registerPropertyChangedListener(listener);

        // Test.
        assertEquals((Integer)0, _vm.getPort());

        _vm.setPort(1234);
        assertEquals((Integer)1234, _vm.getPort());
        assertEquals(1, listener.changedPropertyNames.size());
        assertEquals(LoginViewModel.PortProperty, listener.changedPropertyNames.get(0));
    }

    @Test
    public void username_property_test() {
        // Listener.
        PropertyChangedListenerImpl listener = new PropertyChangedListenerImpl();
        _vm.registerPropertyChangedListener(listener);

        // Test.
        assertEquals("", _vm.getUsername());

        _vm.setUsername("ivan");
        assertEquals("ivan", _vm.getUsername());
        assertEquals(1, listener.changedPropertyNames.size());
        assertEquals(LoginViewModel.UsernameProperty, listener.changedPropertyNames.get(0));
    }

    @Test
    public void login_test() {
        // Set parameter.
        _vm.setHost("localhost");
        _vm.setPort(1234);
        _vm.setUsername("ivan");

        // Test.
        _vm.login();

        // Assert.
        assertEquals("localhost", _chatService.connectHost);
        assertEquals(1234, _chatService.connectPort);
        assertEquals("ivan", _chatService.connectUsername);
    }

    @Test
    public void login_handleError() {
        // Set parameter.
        _vm.setHost("localhost");
        _vm.setPort(1234);
        _vm.setUsername("ivan");

        // Test.
        _chatService.connectException = new Exception("WOOF");
        _vm.login();

        // Assert.
        assertEquals("Error", _modalService.showErrorTitle);
        assertEquals("WOOF", _modalService.showErrorMessage);
    }

    @Test
    public void login_validateHost() {
        _vm.setPort(1234);
        _vm.setUsername("ivan");

        // Empty host.
        _vm.login();
        assertEquals("Host is empty", _modalService.showErrorMessage);
        _modalService.reset();

        // Invalid character.
        _vm.setHost("local host");
        _vm.login();
        assertEquals("Invalid host name", _modalService.showErrorMessage);
        _modalService.reset();

        assertNull(_chatService.connectHost);
        assertEquals(0, _chatService.connectPort);
        assertNull(_chatService.connectUsername);

        // Good.
        _vm.setHost("localhost");
        _vm.login();
        assertNull(_modalService.showErrorMessage);

        _vm.setHost("127.0.0.1");
        _vm.login();
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void login_validatePort() {
        _vm.setHost("localhost");
        _vm.setUsername("ivan");

        // Port 0.
        _vm.login();
        assertEquals("Invalid port number", _modalService.showErrorMessage);
        _modalService.reset();

        // Port negative.
        _vm.setPort(-1);
        _vm.login();
        assertEquals("Invalid port number", _modalService.showErrorMessage);
        _modalService.reset();

        // Port over 65535.
        _vm.setPort(65536);
        _vm.login();
        assertEquals("Invalid port number", _modalService.showErrorMessage);
        _modalService.reset();

        assertNull(_chatService.connectHost);
        assertEquals(0, _chatService.connectPort);
        assertNull(_chatService.connectUsername);

        // Good.
        _vm.setPort(65535);
        _vm.login();
        assertNull(_modalService.showErrorMessage);
    }

    @Test
    public void login_validateUsername() {
        _vm.setHost("localhost");
        _vm.setPort(1234);

        // Empty username.
        _vm.login();
        assertEquals("Username is empty", _modalService.showErrorMessage);
        _modalService.reset();

        assertNull(_chatService.connectHost);
        assertEquals(0, _chatService.connectPort);
        assertNull(_chatService.connectUsername);

        // Good.
        _vm.setUsername("ivan");
        _vm.login();
        assertNull(_modalService.showErrorMessage);
    }
}
