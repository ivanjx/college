package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

public class PropertyChangedListenerImpl implements PropertyChangedListener {

    public ArrayList<String> changedPropertyNames;

    public PropertyChangedListenerImpl() {
        changedPropertyNames = new ArrayList<String>();
    }

    @Override
    public void onPropertyChanged(ViewModelBase sender, String name) {
        changedPropertyNames.add(name);
    }
    
}
