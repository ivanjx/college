package com.ivan.socketchat.client.logic;

public class FakeModalServiceImpl implements ModalService {

    public String showErrorTitle;
    public String showErrorMessage;

    @Override
    public void showError(String title, String message) {
        showErrorTitle = title;
        showErrorMessage = message;
    }

    public void reset() {
        showErrorTitle = null;
        showErrorMessage = null;
    }
    
}
