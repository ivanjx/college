package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

public class LoginStatusChangedListenerImpl implements LoginStatusChangedListener {

    public ArrayList<Boolean> loginStatusChanges;

    public LoginStatusChangedListenerImpl() {
        loginStatusChanges = new ArrayList<>();
    }

    @Override
    public void onLoginStatusChanged(boolean isLoggedIn) {
        loginStatusChanges.add(isLoggedIn);
    }
    
}
