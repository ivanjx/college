package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

import com.ivan.socketchat.contract.User;

public class UserDisconnectedListenerImpl implements UserDisconnectedListener {

    public ArrayList<User> disconnectedUsers;

    public UserDisconnectedListenerImpl() {
        disconnectedUsers = new ArrayList<User>();
    }

    @Override
    public void onUserDisconnected(User user) {
        disconnectedUsers.add(user);
    }
    
}
