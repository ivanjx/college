package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

import com.ivan.socketchat.contract.Message;

public class NewMessageReceivedListenerImpl implements NewMessageReceivedListener {

    public ArrayList<Message> receivedMessages;

    public NewMessageReceivedListenerImpl() {
        receivedMessages = new ArrayList<Message>();
    }

    @Override
    public void onNewMessageReceived(Message message) {
        receivedMessages.add(message);
    }
    
}
