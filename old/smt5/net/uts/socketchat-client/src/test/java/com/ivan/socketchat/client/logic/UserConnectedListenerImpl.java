package com.ivan.socketchat.client.logic;

import java.util.ArrayList;

import com.ivan.socketchat.contract.User;

public class UserConnectedListenerImpl implements UserConnectedListener {

    public ArrayList<User> connectedUsers;

    public UserConnectedListenerImpl() {
        connectedUsers = new ArrayList<User>();
    }

    @Override
    public void onUserConnected(User user) {
        connectedUsers.add(user);
    }
    
}
