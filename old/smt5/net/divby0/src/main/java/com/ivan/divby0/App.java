package com.ivan.divby0;

public class App 
{
    public static void main( String[] args )
    {
        int zeroInt = 0;
        int anInt = 10;

        try
        {
            int divResult = anInt / zeroInt;
            System.out.println("Hasilnya adalah " + divResult);
        }
        catch (ArithmeticException ex)
        {
            System.out.println("Terjadi pembagian dengan nol");
            System.out.println("Di atas blok penanganan sendiri");
        }
        finally
        {
            System.out.println("Kalimat di finally");
        }

        System.out.println("Kalimat di luar blok try-catch-finally");
    }
}
