package com.ivan.simpleechoserver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Input port server yang diinginkan: ");
        int port = input.nextInt();
        input.close();

        ServerSocket server = new ServerSocket(port);

        try
        {
            InetAddress host = InetAddress.getLocalHost();
            System.out.println("SimpleEchoServer is on at " + host + " with port " + port);

            while (true)
            {
                Socket incoming = server.accept();
                BufferedReader in = new BufferedReader(
                    new InputStreamReader(incoming.getInputStream()));
                PrintWriter out = new PrintWriter(incoming.getOutputStream());
                out.println("Hello this is from the Java SimpleEchoServer.");
                out.println("Enter BYE to exit.");
                out.flush();

                while (true)
                {
                    String inStr = in.readLine();
                    out.println("Echo: " + inStr);
                    out.flush();

                    if (inStr.trim().equals("BYE"))
                    {
                        break;
                    }
                }

                incoming.close();
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
        finally
        {
            server.close();
        }
    }
}
