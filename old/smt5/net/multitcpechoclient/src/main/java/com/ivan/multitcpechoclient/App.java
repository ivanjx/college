package com.ivan.multitcpechoclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class App 
{
    private static final int PORT = 1234;

    public static void main( String[] args )
    {
        try
        {
            InetAddress host = InetAddress.getLocalHost();
            Socket link = new Socket(host, PORT);
            BufferedReader serverIn = new BufferedReader(
                new InputStreamReader(
                    link.getInputStream()));
            PrintWriter serverOut = new PrintWriter(
                link.getOutputStream(),
                true);
            Scanner userIn = new Scanner(System.in);

            while (true)
            {
                System.out.print("Enter message (quit to exit): ");
                String message = userIn.nextLine();
                serverOut.println(message);
                String response = serverIn.readLine();
                System.out.println(response);

                if (message.equals("quit"))
                {
                    break;
                }
            }

            System.out.println("Closing connection");
            link.close();
            userIn.close();
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
    }
}
