package com.ivan.threadprintname.extend;

public class ThreadPrintName extends Thread
{
    public ThreadPrintName(String name)
    {
        super(name);
        start();
    }

    @Override
    public void run() 
    {
        String name = getName();

        for (int i = 0; i < 20; ++i)
        {
            System.out.print(name);

            try
            {
                Thread.sleep(1000);
            }
            catch (Exception ex) { }
        }
    }
}
