package com.ivan.threadprintname.extend;

public class App 
{
    public static void main( String[] args )
    {
        ThreadPrintName p1 = new ThreadPrintName("A");
        ThreadPrintName p2 = new ThreadPrintName("B");
        ThreadPrintName p3 = new ThreadPrintName("C");
        ThreadPrintName p4 = new ThreadPrintName("D");
    }
}
