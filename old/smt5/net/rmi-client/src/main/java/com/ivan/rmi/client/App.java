package com.ivan.rmi.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.ivan.rmi.contract.DataInterface;

public class App 
{
    public static void main( String[] args ) throws RemoteException, MalformedURLException, NotBoundException
    {
        DataInterface data = (DataInterface)Naming.lookup("rmi://localhost:1099/data");
        System.out.println("Client berhasil terkoneksi ke server");

        data.metodeSatu();
        data.metodeDua();

        System.out.println("Client telah selesai");
    }
}
