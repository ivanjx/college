package com.ivan.udpechoserver;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class App 
{
    private static final int PORT = 1234;
    private static final int BUFFER_SIZE = 256;

    public static void main( String[] args )
    {
        try
        {
            System.out.println("Opening port " + PORT + " ...");
            DatagramSocket socket = new DatagramSocket(PORT);
            byte[] buffer = new byte[BUFFER_SIZE];
            int numMessages = 0;

            while (true)
            {
                DatagramPacket pktIn = new DatagramPacket(
                    buffer,
                    buffer.length);
                socket.receive(pktIn);
                String messageIn = new String(
                    pktIn.getData(),
                    0,
                    pktIn.getLength());
                System.out.println("Message received");
                ++numMessages;
                String messageOut = "Message " + numMessages + " : " + messageIn;
                DatagramPacket pktOut = new DatagramPacket(
                    messageOut.getBytes(),
                    messageOut.length(),
                    pktIn.getAddress(),
                    pktIn.getPort());
                socket.send(pktOut);
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
        }
    }
}
