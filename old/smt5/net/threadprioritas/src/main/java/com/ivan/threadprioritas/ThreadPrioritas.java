package com.ivan.threadprioritas;

public class ThreadPrioritas extends Thread 
{
    private int _hitungMundur = 5;
    private volatile double _d = 0;

    public ThreadPrioritas(int prioritas)
    {
        setPriority(prioritas);
        start();
    }

    @Override
    public void run() 
    {
        while (true)
        {
            for (int i = 1; i < 10000000; ++i)
            {
                _d += (Math.PI / Math.E) / (double)i;
            }

            System.out.println(this.toString() + ":" + _hitungMundur + "-->" + _d);

            if (--_hitungMundur == 0)
            {
                return;
            }
        }
    }
}
