package com.ivankara.tugastopik15;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SecureHash {
    private String _algorithm;

    public SecureHash(String algorithm) {
        _algorithm = algorithm;
    }

    public byte[] doHash(byte[] data) throws NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());
        MessageDigest md = MessageDigest.getInstance(_algorithm);
        md.update(data);
        return md.digest();
    }
}
